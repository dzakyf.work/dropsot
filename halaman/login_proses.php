<head>
	<link rel="stylesheet" type="text/css" href="../css/welcome.css">
    <link rel="stylesheet" type="text/css" href="../css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
</head>
<body>
<script src="../css/js/jquery.min.js"></script>
<script src="../css/js/bootstrap.min.js"></script>
<script src="../css/js/sweetalert2.min.js"></script>

<?php 
// mengaktifkan session pada php
session_start();

// menghubungkan php dengan koneksi database
include '../koneksi.php';

// menangkap data yang dikirim dari form login
$username = $_POST['username'];
$password = $_POST['password'];


// menyeleksi data user dengan username dan password yang sesuai
$login = mysqli_query($koneksi,"select * from dropshipper where username='$username' and password='$password'");
// menghitung jumlah data yang ditemukan
$cek = mysqli_num_rows($login);

// cek apakah username dan password di temukan pada database
if($cek > 0){

	$data = mysqli_fetch_assoc($login);
	$_SESSION['id_ds'] = $data['id_ds'];

	// cek jika user login sebagai admin
	if($data['level']=="1"){
		// buat session login dan username
		$_SESSION['username'] = $username;
		$_SESSION['level'] = "1";
		// alihkan ke halaman dashboard admin
		header("location:../halaman1/indexlv1.php?page=home");

	// cek jika user login sebagai pegawai
	}else if($data['level']=="2"){
		// buat session login dan username
		$_SESSION['username'] = $username;
		$_SESSION['level'] = "2";

		// alihkan ke halaman dashboard pegawai
		setcookie("id_ds", $data['id_ds'], time()+3600, "/");
		header("location:../halaman2/indexlv2.php");

	// cek jika user login sebagai pengurus
	}else if($data['level']=="3"){
		// buat session login dan username
		$_SESSION['username'] = $username;
		$_SESSION['level'] = "3";
		// alihkan ke halaman dashboard pengurus
		header("location:../admin-ds/index.php");

	}
		else if($data['level']=="4"){
		// buat session login dan username
		$_SESSION['username'] = $username;
		$_SESSION['level'] = "4";
		// alihkan ke halaman dashboard pengurus
		header("location:kabid/kabidadmin.html");
		}
		

	
	else{

		// alihkan ke halaman login kembali
		
		header("refresh:4;url=../index.php?page=login"); 
            echo "<script>Swal.fire({type: 'error', title: 'Login Gagal', text: 'Password atau Username Salah'});</script>";
	}

	
}else{
	header("refresh:4;url=../index.php?page=login"); 
            echo "<script>Swal.fire({type: 'error', title: 'Login Gagal', text: 'Password atau Username Salah'});</script>";
}



?>
</body>