<head>
	<link rel="stylesheet" type="text/css" href="../css/welcome.css">
    <link rel="stylesheet" type="text/css" href="../css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
</head>
<body>
<script src="../css/js/jquery.min.js"></script>
<script src="../css/js/bootstrap.min.js"></script>
<script src="../css/js/sweetalert2.min.js"></script>

<?php
if($_POST){
 
    // include database connection
    include '../koneksi.php';
 
    try{
     
        // insert query
        $query = "INSERT INTO dropshipper SET nama_ds=:nama_ds, gender=:gender, alamat=:alamat, no_hp=:no_hp, email=:email, username=:username, password=:password, level=:level";
 
        // prepare query for execution
        $stmt = $con->prepare($query);
 
        // posted values
        $nama_ds=htmlspecialchars(strip_tags($_POST['nama_ds']));
        $gender=htmlspecialchars(strip_tags($_POST['gender']));
        $alamat=htmlspecialchars(strip_tags($_POST['alamat']));
        $no_hp=htmlspecialchars(strip_tags($_POST['no_hp']));
        $email=htmlspecialchars(strip_tags($_POST['email']));
        $username=htmlspecialchars(strip_tags($_POST['username']));
        $password=htmlspecialchars(strip_tags($_POST['password']));
        $level=htmlspecialchars(strip_tags($_POST['level']));

 
        // bind the parameters
        $stmt->bindParam(':nama_ds', $nama_ds);
        $stmt->bindParam(':gender', $gender);
        $stmt->bindParam(':alamat', $alamat);
        $stmt->bindParam(':no_hp', $no_hp);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':password', $password);
        $stmt->bindParam(':level', $level);


         
        // Execute the query
        if($stmt->execute()){
            header("refresh:5;url=../index.php?page=login"); 
            echo "<script>Swal.fire('Terimakasih telah mendaftar, Silahkan LOGIN');</script>";
        }else{
            header("refresh:2;url=../index.php"); 
            echo "<script>Swal.fire('Pendaftaran Gagal!');</script>";
        }
         
    }
     
    // show error
    catch(PDOException $exception){
        die('ERROR: ' . $exception->getMessage());
    }
}
?>

<script type=”text/javascript”></script>
</body>