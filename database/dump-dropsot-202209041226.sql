-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: dropsot
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.24-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dropshipper`
--

DROP TABLE IF EXISTS `dropshipper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dropshipper` (
  `id_ds` int(100) NOT NULL AUTO_INCREMENT,
  `nama_ds` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_hp` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` int(100) NOT NULL,
  `no_rek` int(100) NOT NULL,
  `nama_rek` varchar(100) NOT NULL,
  `bank` varchar(100) NOT NULL,
  `foto_ktp` varchar(100) NOT NULL,
  PRIMARY KEY (`id_ds`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dropshipper`
--

LOCK TABLES `dropshipper` WRITE;
/*!40000 ALTER TABLE `dropshipper` DISABLE KEYS */;
INSERT INTO `dropshipper` VALUES (2,'adipeng','Laki-Laki','tajug','085879887675','adi.peng007@gmail.com','adi','adi',2,42342342,'adi','BRI','2.png'),(3,'zaki','Perempuan','asadsad','213124124','sadadad@gmail.com','zaki','zaki',2,1234567,'test','BRI','a3MRRBKN_700w_0.jpg'),(5,'admin','Laki-Laki','Jl. Admin no. 1','0857444888999','admin@gmail.com','admin','admin',3,0,'admin','BRI','');
/*!40000 ALTER TABLE `dropshipper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gudang_informasi`
--

DROP TABLE IF EXISTS `gudang_informasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gudang_informasi` (
  `id_inf` int(11) NOT NULL AUTO_INCREMENT,
  `nama_inf` text NOT NULL,
  `file_dok` text NOT NULL,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`id_inf`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gudang_informasi`
--

LOCK TABLES `gudang_informasi` WRITE;
/*!40000 ALTER TABLE `gudang_informasi` DISABLE KEYS */;
INSERT INTO `gudang_informasi` VALUES (28,'Informasi 1','http://localhost/dropshot/upload/239-897-1-PB (1).pdf','Deskripsi dari informasi 1 ');
/*!40000 ALTER TABLE `gudang_informasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produk`
--

DROP TABLE IF EXISTS `produk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL AUTO_INCREMENT,
  `nama_produk` text NOT NULL,
  `deskripsi` text NOT NULL,
  `harga_pokok` int(11) NOT NULL,
  `foto_produk` text DEFAULT NULL,
  `harga_jual` int(11) DEFAULT NULL,
  `berat` int(11) DEFAULT 0,
  PRIMARY KEY (`id_produk`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produk`
--

LOCK TABLES `produk` WRITE;
/*!40000 ALTER TABLE `produk` DISABLE KEYS */;
INSERT INTO `produk` VALUES (10,'jahe','jahe asli buatan nederland',20000,'http://localhost/dropshot/foto/upload/a614Oq2_460swp.jpeg',30000,1),(12,'Buah','Buah asli buatan nederland',10000,'http://localhost/dropshot/foto/upload/a614Oq2_460swp.jpeg',25000,1),(13,'Rempah ','Rempah asli buatan nederland',12000,'http://localhost/dropshot/foto/upload/a614Oq2_460swp.jpeg',24000,2);
/*!40000 ALTER TABLE `produk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi`
--

DROP TABLE IF EXISTS `transaksi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL AUTO_INCREMENT,
  `status_transaksi` enum('BELUM_DIPROSES','SEDANG_DIPROSES','DIKIRIM','SELESAI_DIPROSES') DEFAULT NULL,
  `nama_pembeli` text DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `no_hp` text DEFAULT NULL,
  `nama_pengirim` text DEFAULT NULL,
  `no_hp_pengirim` text DEFAULT NULL,
  `jasa_pengiriman` text DEFAULT NULL,
  `grand_total` int(11) DEFAULT NULL,
  `photo_dikirim_path` text DEFAULT NULL,
  `no_resi` varchar(100) DEFAULT NULL,
  `harga_jual` int(11) DEFAULT NULL,
  `id_dropshipper` int(11) DEFAULT NULL,
  `status_pencairan` enum('BELUM_DICAIRKAN','MENUNGGU_KONFIRMASI','DICAIRKAN') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `bukti_transfer_path` text DEFAULT NULL,
  PRIMARY KEY (`id_transaksi`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi`
--

LOCK TABLES `transaksi` WRITE;
/*!40000 ALTER TABLE `transaksi` DISABLE KEYS */;
INSERT INTO `transaksi` VALUES (25,'SELESAI_DIPROSES','pembeli 2','alamat ','023809283923','pengirim 2','213891873','kantor_pos',30000,'http://localhost/dropshot/foto/upload/brg_dkrm_6186f918ecdd7ea6db937c57d4b0bb22.jpg','09867867XD',50000,3,'BELUM_DICAIRKAN','2022-08-19 18:23:01',NULL),(26,'SELESAI_DIPROSES','pembeli 3','alamat ','0237428374','pengirim 3','073847928347','kantor_pos',90000,'http://localhost/dropshot/foto/upload/brg_dkrm_FOVXVjlXsAosFLk.jpg','KOPL1234',100000,3,'DICAIRKAN','2022-08-19 18:23:01',NULL),(27,'BELUM_DIPROSES','Pembeli 1','alamat ','085788999666','Pengirim 1 ','089888777999','ninja_express',10000,'','085647XDSA',50000,3,'DICAIRKAN','2022-08-21 06:44:25','http://localhost/dropshot/foto/upload/bkt_trans_FOVXVjlXsAosFLk.jpg'),(28,'DIKIRIM','Pembeli 4','Alamat ','085644777333','Pengirim 4','085422233322','kantor_pos',10000,'http://localhost/dropshot/foto/upload/brg_dkrm_FOVXVjlXsAosFLk.jpg','123456',45000,3,'BELUM_DICAIRKAN','2022-08-24 14:37:33',NULL);
/*!40000 ALTER TABLE `transaksi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_barang`
--

DROP TABLE IF EXISTS `transaksi_barang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaksi_barang` (
  `id_barang` int(11) NOT NULL,
  `id_transaksi` int(11) DEFAULT NULL,
  `nama` text DEFAULT NULL,
  `berat` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `total_harga` int(11) DEFAULT NULL,
  `harga_satuan` int(11) DEFAULT NULL,
  `id_dropshipper` int(11) DEFAULT NULL,
  `id_transaksi_barang` int(11) NOT NULL AUTO_INCREMENT,
  `catatan` text DEFAULT NULL,
  PRIMARY KEY (`id_transaksi_barang`),
  KEY `id_transaksi` (`id_transaksi`),
  CONSTRAINT `transaksi_barang_ibfk_1` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi` (`id_transaksi`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_barang`
--

LOCK TABLES `transaksi_barang` WRITE;
/*!40000 ALTER TABLE `transaksi_barang` DISABLE KEYS */;
INSERT INTO `transaksi_barang` VALUES (12,25,'Buah',0,12,'Buah asli buatan nederland',25000,25000,3,12,NULL),(13,26,'Rempah ',2,2,'Rempah asli buatan nederland',48000,24000,3,13,NULL),(12,27,'Buah',1,1,'Buah asli buatan nederland',10000,10000,3,19,NULL),(12,28,'Buah',1,4,'Buah asli buatan nederland',40000,10000,3,20,NULL),(10,NULL,'jahe',1,1,'jahe asli buatan nederland',20000,20000,3,22,NULL);
/*!40000 ALTER TABLE `transaksi_barang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'dropsot'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-04 12:26:06
