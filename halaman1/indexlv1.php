<?php
session_start(); 
if (!isset($_SESSION["username"])) {
	header("location: ../index.php?page=login");
	exit;
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>Dropship Knalpot Purbalingga</title>
  <!-- menghubungkan dengan file css -->
  <link rel="stylesheet" type="text/css" href="../css/lv1.css">
    <!-- menghubungkan dengan file jquery -->
  <script type="text/javascript" src="jquery.js"></script>

  <div class="menu">
    <ul>
    <div class="logo"> <a href="indexlv1.php?page=home"> <img src="../foto/log.png" /></a></div>
    <li><a href="../logout.php">LOGOUT</a></li>      
    <li><a href="indexlv1.php?page=aboutlv1">TENTANG KAMI</a></li>
    <li><a href="indexlv1.php?page=snklv1">S&K DROPSHIP</a></li>
    <li><a href="indexlv1.php?page=home">Beranda</a></li>
    </ul>
  </header>
</head>
<body>
  <header class="content"> 
  <div class="badan">
 
 
  <?php 
  if(isset($_GET['page'])){
    $page = $_GET['page'];
 
    switch ($page) {
      case 'home':
        include "homelv1.php";
        break;  
        case 'lengkapi':
        include "lengkapi.php";
        break;  
        case 'lengkap':
        include "lengkap.php";
        break;  
        case 'aboutlv1':
        include "aboutlv1.php";
        break; 
        case 'snklv1':
        include "snklv1.php";
        break; 
      default:
        echo "<center><h3>Maaf. Halaman tidak di temukan !</h3></center>";
        break;
    }
  }else{
    include "homelv1.php";

  }
 
   ?>
 
  </div>
</body>
</html>