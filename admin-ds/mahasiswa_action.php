<?php
session_start();
include 'koneksii.php';
include 'csrf.php';

function anti($text){
	return $id_ds = stripslashes(strip_tags(htmlspecialchars($text, ENT_QUOTES)));
}


if (isset($_POST['jenis']) && $_POST['jenis']=="view_data") {
	$data = [];
	$gender = '%' . anti($_POST['gender']) . '%';
	$keyword = '%' . anti($_POST['keyword']) . '%';

	$query = "SELECT * FROM dropshipper WHERE gender LIKE ? AND nama_ds LIKE ? ORDER BY nama_ds ASC";
	$dewan1 = $db1->prepare($query);
	$dewan1->bind_param('ss', $gender, $keyword);
	$dewan1->execute();
	$res1 = $dewan1->get_result();
	while ($row = $res1->fetch_assoc()) {
	    $data[] = $row;
	}
	echo json_encode($data);
}

if (isset($_POST['jenis']) && $_POST['jenis']=="view_data_by_id") {
	$id_ds = anti($_POST['id_ds']);
	$query = "SELECT * FROM dropshipper WHERE id_ds=? ORDER BY nama_ds ASC";
	$dewan1 = $db1->prepare($query);
	$dewan1->bind_param('i', $id_ds);
	$dewan1->execute();
	$res1 = $dewan1->get_result();
	while ($row = $res1->fetch_assoc()) {
	    $h['id_ds'] = $row["id_ds"];
	    $h['nama_ds'] = $row["nama_ds"];
		$h['alamat'] = $row["alamat"];
	    $h['gender'] = $row["gender"];
	    $h['no_hp'] = $row["no_hp"];
	    $h['email'] = $row["email"];

	}
	echo json_encode($h);
} 


if (isset($_POST['jenis']) && $_POST['jenis']=="edit_data") {
	$id_ds = anti($_POST['id_ds']);
	$nama_ds = anti($_POST['nama_ds']);
	$gender = anti($_POST['gender']);
	$alamat = anti($_POST['alamat']);
	$no_hp = anti($_POST['no_hp']);
	$email = anti($_POST['email']);

	$query = "UPDATE dropshipper SET nama_ds=?, gender=?, alamat=?, no_hp=?, email=? WHERE id_ds=?";
	$dewan1 = $db1->prepare($query);
	$dewan1->bind_param('sssssi', $nama_ds, $gender, $alamat, $no_hp, $email, $id_ds);
	$dewan1->execute();

	echo json_encode(['code' => 200, 'status' => 'success', 'message' => 'Data Berhasil Diubah']);
}

if (isset($_POST['jenis']) && $_POST['jenis']=="delete_data") {
	$id_ds = anti($_POST['id_ds']);


	$query = "SELECT * FROM dropshipper WHERE id_ds=? LIMIT 0";
    $dewan1 = $db1->prepare($query);
    $dewan1->bind_param('i', $id_ds);
    $dewan1->execute();
    $res1 = $dewan1->get_result();


	$query = "DELETE FROM dropshipper WHERE id_ds=?";
	$dewan1 = $db1->prepare($query);
	$dewan1->bind_param("i", $id_ds);
	$dewan1->execute();

	echo json_encode(['code' => 200, 'status' => 'success', 'message' => 'Data Berhasil Dihapus']);
}

$db1->close();
?>