<?php
//Menggabungkan dengan file koneksi yang telah kita buat
include 'koneksi.php';
include 'auth.php';
include '../api/api_produk.php'
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="icon" href="dk.png">
    <!-- Title Page-->
    <title>Dashboard Admin</title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/theme.css" rel="stylesheet" media="all">
    <!-- Csrf Token -->
	<meta name="csrf-token" content="<?= $_SESSION['csrf_token'] ?>">
	<!-- Bootstrap -->

    <!-- SweetAlert -->
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert2.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    
    <!-- Fontfaces CSS-->
    <link href="assets/hd/css/font-face.css" rel="stylesheet" media="all">
    <link href="assets/hd/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="assets/hd/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="assets/hd/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
 

    <!-- Vendor CSS-->
    <link href="assets/hd/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="assets/hd/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="assets/hd/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="assets/hd/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="assets/hd/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="assets/hd/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="assets/hd/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    
    <!-- Datatable -->
    <link rel="stylesheet" type="text/css" href="assets/css/datatables/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.bootstrap4.min.css">
    <!-- Animasi -->
<link rel="stylesheet" href="assets/css/animate-3.5.2.min.css">
    <style type="text/css">
    	.preloader-single{
			background: #fff;
		    width: 100%;
		    height: 350px;
		    padding: 20px;
		}
		.preloader {
		    position: fixed;
		    width: 100%;
		    height: 100%;
		    z-index: 9999;
		    background-color: #fff;
		}
		.preloader .loading {
		    position: absolute;
		    left: 50%;
		    top: 50%;
		    transform: translate(-50%,-50%);
		    font: 14px arial;
		}
		.preloader .loading p {
		    font-size: 16px;
		    font-weight: bold;
		}
    </style>
</head>

<body class="animsition">
    <div class="page-wrapper">    
    

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <img src="images/logo.png" alt="Dropshot" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="">
                            <a class="js-arrow" href="#">
                            <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="index.php">Dashboard 1</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="js-arrow" href="produk.php">
                                <i class="fas fa-tag"></i>Produk</a>
                                <!-- <ul class="list-unstyled navbar__sub-list js-sub-list">
                                    <li>
                                        <a href="#">Daftar Produk</a>
                                    </li>
                                    <li>
                                        <a href="#">Tambah Data Produk</a>
                                    </li>
                                </ul> -->
                        </li>
                        <li>
                        <a class="js-arrow" href="#">
                       <i class="fas fa-file"></i>Data Transaksi</a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                    <li>
                                        <a href="pesanan_masuk.php">Pesanan Masuk </a>
                                    </li>
                                    <li>
                                <a href="pesanan_diproses.php">Pesanan Diproses</a>
                            </li>
                            <li>
                                <a href="pesanan_dikirim.php">Pesanan Dikirim</a>
                            </li>
                                                                <li>
                                <a href="pesanan_selesai.php">Pesanan Selesai</a>
                            </li>
                            </ul>
                        </li>
                        <li>
                            <a class="js-arrow" href="gudang_informasi.php">
                                <i class="fas fa-lightbulb-o"></i>Gudang Informasi</a>
                        </li>
                        <li>
                        <li class="active has-sub">
						<a class="js-arrow" href="#">
                                <i class="fas fa-lightbulb-o"></i>Data Dropshipper</a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                    <li>
                                        <a href="data-ds-level1.php">Level 1</a>
                                    </li>
                                    <li>
                                        <a href="data-ds-level2.php">Level 2</a>
                                    </li>
                            	</ul>
</li>
                        </li>
                        <li>
                            <a href="laporan.php">
                                <i class="fas fa-print"></i>Laporan</a>
                        </li>
						 <li>
                            <a href="fee_transaksi.php">
                                <i class="fas fa-print"></i>Fee Transaksi</a>
                        </li>
                    
                        
                    </ul>
                </nav>
            </div>
        </aside>
  <!-- END MENU SIDEBAR-->
        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                               
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                    
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="images/icon/admin.png" alt="John Doe" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#">Admin</a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="images/icon/admin.png" alt="John Doe" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5>
                                                        <br>Admin Dropshot
                                                    </h5>
       
                                                </div>
                                            </div>
                                            <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__footer">
                                                <a href="../logout.php">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER Conten-->
		<div class="modal fade" id="modalCreateProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" width="100%">
            <div class="modal-dialog modal-xl rotateInDownLeft animated">
                <div class="modal-content">
                    <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <form method="post" class="form-data" id="form-data">
                    	<div class="row">
                    		<div class="col-sm-12">
                    			<div class="form-group">
									<label>Nama Produk</label>
									<input type="text" name="create_nama_produk" id="create_nama_produk" class="form-control wajib" required>
									<p class="text-danger" id="err_nama_ds"></p>
								</div>
								<div class="form-group">
									<label>Harga Pokok</label>
									<input name="create_harga_pokok" id="create_harga_pokok" class="form-control wajib" required></input>
									<p class="text-danger" id="#"></p>
								</div>
								<div class="form-group">
									<label>Harga Jual</label>
									<input name="create_harga_jual" id="create_harga_jual" class="form-control wajib" required></input>
									<p class="text-danger" id="err_alamat"></p>
								</div>
								<div class="form-group">
								<label>Deskripsi</label>
									<textarea name="creaste_deskripsi" id="create_deskripsi" class="form-control wajib" required></textarea>
									<p class="text-danger" id="err_no_hp"></p>
								</div>
								<div class="form-group">
									<label>Upload gambar</label>
									<input name="create_upload_gambar" id="create_upload_gambar" type="file" class="form-control wajib" required></input>
								</div>
                    		</div>
                    	</div>
                        
						<div class="form-group">
							<button type="button" name="btnSimpanCreateProduct" id="btnSimpanCreateProduct" class="btn btn-primary">
								<i class="fa fa-save"></i> Simpan
							</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								<i class="fa fa-times"></i> Close
							</button>
						</div>

                        <div class="box-footer"></div>
                    </form>
               </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modaltambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" width="100%">
            <div class="modal-dialog modal-xl rotateInDownLeft animated">
                <div class="modal-content">
                    <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <form method="post" class="form-data" id="form-data">
                    	<div class="row">
                    		<div class="col-sm-12">
                    			<div class="form-group">
									<label>Nama Produk</label>
									<input type="text" name="nama_produk" id="nama_produk" class="form-control wajib" required="true">
									<p class="text-danger" id="err_nama_ds"></p>
								</div>
								<div class="form-group">
									<label>Harga Pokok</label>
									<input name="harga_pokok" id="harga_pokok" class="form-control wajib" required="true"></input>
									<p class="text-danger" id="#"></p>
								</div>
								<div class="form-group">
									<label>Harga Jual</label>
									<input name="harga_jual" id="harga_jual" class="form-control wajib" required="true"></input>
									<p class="text-danger" id="err_alamat"></p>
								</div>
								<div class="form-group">
								<label>Deskripsi</label>
									<textarea name="deskripsi" id="deskripsi" class="form-control wajib" required="true"></textarea>
									<p class="text-danger" id="err_no_hp"></p>
								</div>
								<div class="form-group">
									<label>Upload gambar</label>
									<input name="upload_gambar" id="upload_gambar" type="file" class="form-control wajib" required="true"></input>
								</div>
                    		</div>
                    	</div>
                        
						<div class="form-group">
							<button type="button" name="simpan" id="btnSimpan" class="btn btn-primary">
								<i class="fa fa-save"></i> Simpan
							</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								<i class="fa fa-times"></i> Close
							</button>
						</div>

                        <div class="box-footer"></div>
                    </form>
               </div>
                </div>
            </div>
        </div>
		
        <div id="viewModal" class="modal fade mr-tp-100" role="dialog">
		    <div class="modal-dialog modal-lg rollIn animated">
		        <div class="modal-content">
		            <div class="modal-body">
		            	<button type="button" class="close" data-dismiss="modal" >
		                    <span aria-hidden="true">&times;</span>
		                    <span class="sr-only">Close</span>
		                </button>

						<input type="hidden" id="id_transaksi"/>
		            	<div class="container row">
							<div class="col-sm-6">
								<div class="form-group">
									<label><strong>Nama Dropshipper</strong></label>
									<p id="pNamaDropshipper"></p>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label><strong>Nama Pembeli</strong></label>
									<p id="pNamaPembeli"></p>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label><strong>Status Transaksi</strong></label>
									<p id="pStatusTransaksi"></p>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label><strong>No. HP Pembeli</strong></label>
									<p id="pNoHPPembeli"></p>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label><strong>Jasa Pengiriman</strong></label>
									<p id="pJasaPengiriman"></p>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label><strong>Alamat</strong></label>
									<p id="pAlamat"></p>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label><strong>Total Pembayaran</strong></label>
									<p id="pTotalPembayaran"></p>
								</div>
							</div>
							<div class="col-lg-12">
								<label><strong>Pesanan</strong></label>
								<div class="col-lg-12 items">
								</div>
							</div>	
							
						</div>
		            </div>
		        </div>
		    </div>
		</div>
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <h2 class="title-1">Data Pesanan Selesai</h2>
                                </div>
                            </div>
                        </div>
		 	      <div name="s_gender" id="s_gender" ></div>
				      <div type="text" name="s_keyword" id="s_keyword" ></div>
				      
		<div>
		 	<fieldset class="border p-3">
				<!-- <button type="button" id="createProduct" name="createProduct" class="btn btn-primary">Tambah Produk</button> -->
		    	<table id="dataTable" class="table table-striped table-bordered" width="100%"></table>
			</fieldset>
		</div>
		
    </div>

    <div class="text-center mt-5">© <?php echo date('Y'); ?> Copyright:
	    <a> Kenshin</a>
	</div>
       
                                <!-- DATA TABLE-->

    <!-- JQuery -->
    <script src="assets/js/jquery-3.3.1.min.js"></script>
<!-- Bootstrap -->
<script src="assets/js/bootstrap-4.3.1.min.js"></script>
<script src="assets/js/popper.min.js"></script>

    <!-- DataTable -->
    <script src="assets/js/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/js/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="assets/js/datatables/dataTables.buttons.min.js"></script>
    <script src="assets/js/datatables/buttons.bootstrap4.min.js"></script>
   	<script src="assets/js/datatables/buttons.html5.min.js"></script>
   	<script src="assets/js/datatables/buttons.print.min.js"></script>
   	<script src="assets/js/datatables/buttons.colVis.min.js"></script>
   	<script src="assets/js/jszip.min.js"></script>
   	<script src="assets/js/pdfmake.min.js"></script>
   	<script src="assets/js/vfs_fonts.js"></script>

    <!-- CKEditor -->
    <script src="assets/ckeditor/ckeditor.js"></script>

    <!-- Sweet Alert -->
   <script src="assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript">
  		$(document).ready(function(){
  			$(".preloader").fadeOut();
			//Mengirimkan Token Keamanan
			$.ajaxSetup({
			    headers : {
			        'Csrf-Token': $('meta[name="csrf-token"]').attr('content')
			    }
			});

  			let s_Id_ds = "";
  			let table = "";
			let id_produk = "";
  			setDatatable();

			function getPhotoProduct(id){
				return $.ajax({
					'url' : `../api/api_produk.php?action=get_product_by_id&id=${id}`,
					'type': "GET",
				})
			}

			function getNamaDropshipper(id){
				return $.ajax({
					'url': `../api/api_dropshipper.php?action=get_dropshipper_by_id&id_dropshipper=${id}`,
					'type' : "GET",
					'async' : false
				})
			}

			function selesaikanPesanan(id){
				return $.ajax({
					'url': `../api/api_transaksi.php?action=admin_update_transaksi_selesai&id_transaksi=${id}`,
					'type': "POST",
				})
			}

  			function setDatatable(){
  				// let jenis = "view_data";
  				let gender = $('#s_gender').val();
  				let keyword = $('#s_keyword').val();
				
  				table = $('#dataTable').DataTable({
			        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		            "destroy": true,
		            "paging": true,
		            "sorting": true,
		            "responsive": true,
			        "ajax": {
			            "type": "GET",
			            "url": `../api/api_transaksi.php?action=admin_get_transaksi_by_status&status=SELESAI_DIPROSES`,
			            // "data": {jenis:jenis, gender:gender, keyword:keyword},
			            "timeout": 120000,
						"dataSrc": "data",
			        },
			        "sAjaxDataProp": "",
			        "width": "100%",
			        "order": [[ 0, "asc" ]],
			        "dom": "Bfrtip",
			        "buttons": [
			        ],
			        "aoColumns": [
			            {
		                    "name": "No", "title": "No",
		                    "data": null,
		                    render: function (data, type, row, meta) {
		                        return meta.row + meta.settings._iDisplayStart + 1;
		                    }
		                },
						{
							"data": null,
		                    "name": "Nama Dropshipper",
		                    "title": "Nama Dropshipper",
		                    "render": function (data, row, type, meta) {
								var promise = getNamaDropshipper(data.id_dropshipper)
								var al = "";
								promise.done(function(data, status, jqXHR){
									datas = data.data[0]
									if(row === "export"){
										al += datas.nama_ds;
									} else {
										al += potongDeskripsi(datas.nama_ds);
									}
								})
		                        return al;
		                    }
						},

		                {
		                    "data": null,
		                    "name": "Nama Pembeli",
		                    "title": "Nama Pembeli",
		                    "render": function (data, row, type, meta) {
		                        var al = "";
		                        if(row === "export"){
		                            al += data.nama_pembeli;
		                        } else {
		                            al += potongDeskripsi(data.nama_pembeli);
		                        }
		                        return al;
		                    }
		                },
						{
		                    "data": null,
		                    "name": "Status Transaksi",
		                    "title": "Status Transaksi",
		                    "render": function (data, row, type, meta) {
		                        var al = "";
		                        if(row === "export"){
		                            al += data.status_transaksi;
		                        } else {
		                            al += potongDeskripsi(data.status_transaksi);
		                        }
		                        return al;
		                    }
		                },
						{
		                    "data": null,
		                    "name": "No. Handphone",
		                    "title": "No. Handphone",
		                    "render": function (data, row, type, meta) {
		                        var al = "";
		                        if(row === "export"){
		                            al += data.no_hp;
		                        } else {
		                            al += potongDeskripsi(data.no_hp);
		                        }
		                        return al;
		                    }
		                },
						{
		                    "data": null,
		                    "name": "Alamat",
		                    "title": "Alamat",
		                    "render": function (data, row, type, meta) {
		                        var al = "";
		                        if(row === "export"){
		                            al += data.alamat;
		                        } else {
		                            al += potongDeskripsi(data.alamat);
		                        }
		                        return al;
		                    }
		                },
		        
			            {
		                    "data": null,
		                    "name": "Aksi",
		                    "title": "Aksi",
		                    "width": "90px",
		                    "render": function (data, row, type, meta) {
			                    return `
								<div>
								<button id_ds="`+data.id_dropshipper+`" id_transaksi="`+data.id_transaksi+`" class="btn btn-primary btn-sm view_data" title="Lihat Data"> <i class="fa fa-search"></i></button>
								<div>
			                    `;
			                }
			            }
			        ]
			    });
				
  				table.buttons().container().appendTo( '#dataTable_wrapper .col-md-6:eq(0)' );

				$('#createProduct').on('click', function(){
					$("#modalCreateProduct").modal("show")

					
				})

				$('#btnSimpanCreateProduct').click(function(){
					insertProduct()
				})


				$('#selesaikan_pesanan').click(function(){
					 Swal.fire({
						title: 'Apakah anda yakin?',
						text: "",
						type:"warning",
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Beli Sekarang'
						}).then((result) => {
							let id_transaksi =$("#id_transaksi").val()
							selesaikanPesanan(id_transaksi);
					}).then(() => {
						window.location.reload()
					})
				})


				$('#dataTable tbody').on('click', '.view_data', function () {
				    let id_ds = $(this).attr('id_ds');
					let id_transaksi = $(this).attr('id_transaksi')
					var get_nama_ds = getNamaDropshipper(id_ds)

				    $("#viewModal").modal("show");
					$(".items-child").remove()
				    $.ajax({
				        type: 'GET',
				        url: `../api/api_transaksi.php?action=get_transaksi_by_id&id_dropshipper=${id_ds}&id_transaksi=${id_transaksi}`,
				        // data: {id_ds:id_ds, jenis:jenis},
				        dataType:'json',
				        success: function(response) {
							get_nama_ds.done(function(data, status, jqXHR){
								$("#pNamaDropshipper").html(data.data[0].nama_ds)
							})
				            $("#pNamaPembeli").html(response.data[0].nama_pembeli);
				            $("#pStatusTransaksi").html(response.data[0].status_transaksi);
				            $("#pNoHPPembeli").html(response.data[0].no_hp);
				            $("#pJasaPengiriman").html(response.data[0].jasa_pengiriman);
							$("#pAlamat").html(response.data[0].alamat)
							$("#pTotalPembayaran").html("Rp. " + response.data[0].grand_total)
							$("#id_transaksi").val(response.data[0].id_transaksi)

							response.data[0].pesanan.forEach(function(item){
							var detail_produk_promise = getPhotoProduct(item.id_barang)
								detail_produk_promise.done(function(data, textStatus, jqXHR){
									var detail = data.data[0]
									items =  
									`
										<div class="card m-2 items-child">
											<div class="card-body">
											<img src="${detail.foto_produk}" class="rounded float-left pr-lg-3" width="80px" height="auto" alt="${detail.nama_produk}">
												<h5 class="card-title">${detail.nama_produk}</h5>
												<h4 class="card-title">Rp. ${detail.harga_jual}</h4>
												<p class="card-text">${detail.deskripsi}</p>
											</div>
										</div>
									`
									$(".items").append(items)
								})
							})
							
				        }
				    });
				});

			    
  			}

			function potongDeskripsi(string){
				let hasil = "";

				if (string=="" || string==null) {
					hasil = "";
				} else if(string.length > 15){
					hasil = string.substr(0, 15) + "...";
				} else {
					hasil = string;
				}
				return hasil;
			}

			$("#btnCari").click(function(){
				setDatatable();
			});

			$("#s_gender").change(function(){
				setDatatable();
			});

			$("#s_keyword").keyup(function(){
				setDatatable();
			});


			$("#btnTambah").click(function(){
				$('#modaltambah').modal({
				    focus: false
				});
			$("#btnTambah").click(function(){
				$('#modaltambah').modal({
				    focus: false
				});
				$('#form-data')[0].reset();

CKEDITOR.instances['nama_ds'].setData("");
s_Id_ds = "";
});
				$('#form-data')[0].reset();

				CKEDITOR.instances['alamat'].setData("");
				s_Id_ds = "";
			});

			$(".wajib").change(function(){
			    let nama_ds = $(this).attr('nama_ds');
			    $('#err_'+nama_ds).html("");
			});

			$("#btnSimpan").click(function(){
			    simpanData();
			});

			$('#form-data').keypress(function (e) {
			    if (e.which == 10) {
			        simpanData();
			    }
			});

		    function simpanData(){
	            // let nama_ds = $('#nama_ds').val();
				// let alamat= $('#alamat').val();
	            // let gender = $('#gender').val();
	            // let no_hp = $('#no_hp').val();
	            // let email = $('#email').val();
	            // let jenis = "";
				let nama_produk = $("#nama_produk").val();
				let harga_pokok = $("#harga_pokok").val();
				let harga_jual = $("#harga_jual").val();
				let deskripsi = $("#deskripsi").val();
				let gambar = document.getElementById("upload_gambar").files[0];
				// console.log(nama_produk, gambar, this)
            	if(s_Id_ds==""){
            		jenis = "edit_data";
            	}
	            if (nama_produk!="" && harga_pokok!=""  && harga_jual!=""  && deskripsi!="" && gambar!="") {
	            	let formData = new FormData();
			        formData.append('nama_produk',nama_produk);
			        formData.append('harga_pokok', harga_pokok);
			        formData.append('harga_jual', harga_jual);
			        formData.append('deskripsi', deskripsi);
			        formData.append('foto', gambar);
			        
					$.ajax({
			            type: 'POST',
			            url: `../api/api_produk.php?action=update_product&id=${id_produk}`,
			            data: formData,
			            cache: false,
		                processData: false,
		                contentType: false,
			            success: function(response) {
			            	if(response.code == 200){
								Swal.fire({
								  type: 'success',
								  title: 'Sukses',
								  text: 'Data Berhasil Disimpan',
								  animation: false,
		  						  customClass: 'animated fadeInDown'
								}).then((result) => {
									$('#dataTable').DataTable().ajax.reload();
				                	$('#modaltambah').modal('hide');

								});
							} else {
								Swal.fire({
								  type: response.status,
								  title: response.message,
								  text: '',
								})
							}
			            },error: function(response) {
			            	console.log(response.responseText);
			            }
			        });
			    }
		    }
		});

	</script>


            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

    <script src="vendor/animsition/animsition.min.js"></script>

    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

</body>

</html>
<!-- end document-->
