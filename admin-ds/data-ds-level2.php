<?php
//Menggabungkan dengan file koneksi yang telah kita buat
include 'koneksi.php';
include 'auth.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="icon" href="dk.png">
    <!-- Title Page-->
    <title>Dashboard Admin</title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/theme.css" rel="stylesheet" media="all">
    <!-- Csrf Token -->
	<meta name="csrf-token" content="<?= $_SESSION['csrf_token'] ?>">
	<!-- Bootstrap -->

    <!-- SweetAlert -->
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert2.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    
    <!-- Fontfaces CSS-->
    <link href="assets/hd/css/font-face.css" rel="stylesheet" media="all">
    <link href="assets/hd/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="assets/hd/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="assets/hd/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
 

    <!-- Vendor CSS-->
    <link href="assets/hd/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="assets/hd/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="assets/hd/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="assets/hd/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="assets/hd/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="assets/hd/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="assets/hd/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    
    <!-- Datatable -->
    <link rel="stylesheet" type="text/css" href="assets/css/datatables/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.bootstrap4.min.css">
    <!-- Animasi -->
<link rel="stylesheet" href="assets/css/animate-3.5.2.min.css">
    <style type="text/css">
    	.preloader-single{
			background: #fff;
		    width: 100%;
		    height: 350px;
		    padding: 20px;
		}
		.preloader {
		    position: fixed;
		    width: 100%;
		    height: 100%;
		    z-index: 9999;
		    background-color: #fff;
		}
		.preloader .loading {
		    position: absolute;
		    left: 50%;
		    top: 50%;
		    transform: translate(-50%,-50%);
		    font: 14px arial;
		}
		.preloader .loading p {
		    font-size: 16px;
		    font-weight: bold;
		}
    </style>
</head>

<body class="animsition">
    <div class="page-wrapper">    
    

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <img src="images/logo.png" alt="Dropshot" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="">
                            <a class="js-arrow" href="#">
                            <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="index.html">Dashboard 1</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="js-arrow" href="produk.php">
                                <i class="fas fa-tag"></i>Produk</a>
                        </li>
                        <li>
                        <a class="js-arrow" href="#">
                       <i class="fas fa-file"></i>Data Transaksi</a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                    <li>
                                        <a href="#">Pesanan Masuk </a>
                                    </li>
                                    <li>
                                <a href="#">Pesanan Diproses</a>
                            </li>
                            <li>
                                <a href="#">Pesanan Dikirim</a>
                            </li>
                                                                <li>
                                <a href="#">Pesanan Selesai</a>
                            </li>
                            </ul>
                        </li>
                        <li>
                            <a class="js-arrow" href="#">
                                <i class="fas fa-lightbulb-o"></i>Gudang Informasi</a>
                        </li>
                        <!-- <li>
                        	<li class="active has-sub">
                            <a href="#">
                                <i class="fas fa-users"></i>Data Dropshipper</a>
							</li>
                        </li> -->
						 <li>
                            <a class="js-arrow" href="#">
                                <i class="fas fa-lightbulb-o"></i>Data Dropshipper</a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                    <li>
                                        <a href="data-ds-level1.php">Level 1</a>
                                    </li>
                                    <li>
                                        <a href="data-ds-level2.php">Level 2</a>
                                    </li>
                                </ul>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fas fa-print"></i>Laporan</a>
                        </li>
						 <li>
                            <a href="fee_transaksi.php">
                                <i class="fas fa-print"></i>Fee Transaksi</a>
                        </li>
                    
                        
                    </ul>
                </nav>
            </div>
        </aside>
  <!-- END MENU SIDEBAR-->
        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                               
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                    
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="images/icon/admin.png" alt="John Doe" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#">Admin</a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="images/icon/admin.png" alt="John Doe" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5>
                                                        <br>Admin Dropshot
                                                    </h5>
       
                                                </div>
                                            </div>
                                            <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__footer">
                                                <a href="../logout.php">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER Conten-->
            <div class="modal fade" id="modaltambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" width="100%">
            <div class="modal-dialog modal-xl rotateInDownLeft animated">
                <div class="modal-content">
                    <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <form method="post" class="form-data" id="form-data">
                    	<div class="row">
                    		<div class="col-sm-6">
                    			<div class="form-group">
									<label>Nama Dropshipper</label>
									<input type="text" name="nama_ds" id="nama_ds" class="form-control wajib" required="true">
									<p class="text-danger" id="err_nama_ds"></p>
								</div>
								<div class="form-group">
									<label>Alamat</label>
									<textarea name="alamat" id="alamat" class="form-control wajib" required="true"></textarea>
									<p class="text-danger" id="err_alamat"></p>
								</div>
								<div class="form-group">
									<label>Gender</label>
									<select name="gender" id="gender" class="form-control wajib" required="true">
										<option value=""></option>
										<option value="Laki-Laki">Laki-Laki</option>
										<option value="Perempuan">perempuan</option>
									</select>
									<p class="text-danger" id="err_gender"></p>
								</div>
								<div class="form-group">
								<label>No HP</label>
									<input name="no_hp" id="no_hp" class="form-control wajib" required="true">
									<p class="text-danger" id="err_no_hp"></p>
								</div>
                    		</div>
                    		<div class="col-sm-6">
                    			<div class="form-group">
								<label>Email</label>
									<input name="email" id="email" class="form-control wajib" required="true">
									<p class="text-danger" id="err_email"></p>
								</div>
                    		</div>
                    	</div>
                        
						<div class="form-group">
							<button type="button" name="simpan" id="btnSimpan" class="btn btn-primary">
								<i class="fa fa-save"></i> Simpan
							</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								<i class="fa fa-times"></i> Close
							</button>
						</div>

                        <div class="box-footer"></div>
                    </form>
               </div>
                </div>
            </div>
        </div>
        <div id="viewModal" class="modal fade mr-tp-100" role="dialog">
		    <div class="modal-dialog modal-lg rollIn animated">
		        <div class="modal-content">
		            <div class="modal-body">
		            	<button type="button" class="close" data-dismiss="modal" >
		                    <span aria-hidden="true">&times;</span>
		                    <span class="sr-only">Close</span>
		                </button>

		            	<div class="container row">
							<div class="col-sm-6">
								<div class="form-group">
									<label><strong>Nama Mahasiswa</strong></label>
									<p id="pNama"></p>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label><strong>Gender</strong></label>
									<p id="pGender"></p>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label><strong>Alamat</strong></label>
									<p id="pAlamat"></p>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label><strong>No Hp</strong></label>
									<p id="pNoHP"></p>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label><strong>email</strong></label>
									<p id="pEmail"></p>
								</div>
							</div>
						</div>
		            </div>
		        </div>
		    </div>
		</div>
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <h2 class="title-1">Data Dropshipper Terdaftar</h2>
                                </div>
                            </div>
                        </div>
		 	      <div name="s_gender" id="s_gender" ></div>
				      <div type="text" name="s_keyword" id="s_keyword" ></div>
				      
		<div>
		 	<fieldset class="border p-3">
		    	<table id="dataTable" class="table table-striped table-bordered" width="100%"></table>
			</fieldset>
		</div>
		
    </div>

    <div class="text-center mt-5">© <?php echo date('Y'); ?> Copyright:
	    <a> Kenshin</a>
	</div>
       
    <!-- DATA TABLE-->

    <!-- JQuery -->
    <script src="assets/js/jquery-3.3.1.min.js"></script>
	<!-- Bootstrap -->
	<script src="assets/js/bootstrap-4.3.1.min.js"></script>
	<script src="assets/js/popper.min.js"></script>

    <!-- DataTable -->
    <script src="assets/js/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/js/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="assets/js/datatables/dataTables.buttons.min.js"></script>
    <script src="assets/js/datatables/buttons.bootstrap4.min.js"></script>
   	<script src="assets/js/datatables/buttons.html5.min.js"></script>
   	<script src="assets/js/datatables/buttons.print.min.js"></script>
   	<script src="assets/js/datatables/buttons.colVis.min.js"></script>
   	<script src="assets/js/jszip.min.js"></script>
   	<script src="assets/js/pdfmake.min.js"></script>
   	<script src="assets/js/vfs_fonts.js"></script>

    <!-- CKEditor -->
    <script src="assets/ckeditor/ckeditor.js"></script>

    <!-- Sweet Alert -->
   <script src="assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript">
  		$(document).ready(function(){
  			$(".preloader").fadeOut();
			//Mengirimkan Token Keamanan
			$.ajaxSetup({
			    headers : {
			        'Csrf-Token': $('meta[name="csrf-token"]').attr('content')
			    }
			});

  			let s_Id_ds = "";
  			let table = "";
  			setDatatable();
  			function setDatatable(){
  				let jenis = "view_data_by_level";
  				let gender = $('#s_gender').val();
  				let keyword = $('#s_keyword').val();

  				table = $('#dataTable').DataTable({
			        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		            "destroy": true,
		            "paging": true,
		            "sorting": true,
		            "responsive": true,
			        "ajax": {
			            "type": "GET",
			            "url": "../api/api_dropshipper.php?action=get_dropshipper_by_level&level=2",
			            "timeout": 120000,
			            "dataSrc": "data",
			        },
			        "sAjaxDataProp": "",
			        "width": "100%",
			        "order": [[ 0, "asc" ]],
			        "dom": "Bfrtip",
			        "buttons": [
			        ],
			        "aoColumns": [
			            {
		                    "name": "No", "title": "No",
		                    "data": null,
		                    render: function (data, type, row, meta) {
		                        return meta.row + meta.settings._iDisplayStart + 1;
		                    }
		                },

		                {
		                    "data": null,
		                    "name": "Nama Dropshipper",
		                    "title": "Nama Dropshipper",
		                    "render": function (data, row, type, meta) {
		                        var al = "";
		                        if(row === "export"){
		                            al += data.nama_ds;
		                        } else {
		                            al += potongDeskripsi(data.nama_ds);
		                        }
		                        return al;
		                    }
		                },
						{
		                    "data": null,
		                    "name": "Alamat",
		                    "title": "Alamat",
		                    "render": function (data, row, type, meta) {
		                        var al = "";
		                        if(row === "export"){
		                            al += data.alamat;
		                        } else {
		                            al += potongDeskripsi(data.alamat);
		                        }
		                        return al;
		                    }
		                },
		               	{"data": "gender", "name": "gender", "title": "Gender"},
		               	{"data": "no_hp", "name":  "no_hp", "title": "No Hp"},
		        
			            {
		                    "data": null,
		                    "name": "Aksi",
		                    "title": "Aksi",
		                    "width": "90px",
		                    "render": function (data, row, type, meta) {
			                    return `<button id_ds="`+data.id_ds+`" class="btn btn-primary btn-sm view_data" title="Lihat Data"> <i class="fa fa-search"></i></button>
			                    <button id_ds="`+data.id_ds+`" class="btn btn-success btn-sm edit_data" title="Edit Data"> <i class="fa fa-edit"></i></button>
			                    <button id_ds="`+data.id_ds+`" class="btn btn-danger btn-sm hapus_data" title="Hapus Data"> <i class="fa fa-trash"></i></button>
			                    `;
			                }
			            }
			        ]
			    });
				
  				table.buttons().container().appendTo( '#dataTable_wrapper .col-md-6:eq(0)' );

				$('#dataTable tbody').on( 'click', '.view_data', function () {
				    let id_ds = $(this).attr('id_ds');
				    let jenis = "view_data_by_id";

				    $.ajax({
				        type: 'POST',
				        url: "mahasiswa_action.php",
				        data: {id_ds:id_ds, jenis:jenis},
				        dataType:'json',
				        success: function(response) {
				            $("#pNama").html(response.nama_ds);
				            $("#pAlamat").html(response.alamat);
				            $("#pGender").html(response.gender);
				            $("#pNoHP").html(response.no_hp);
				            $("#pEmail").html(response.email);

				            $("#viewModal").modal("show");
				        }
				    });
				});

			    $('#dataTable tbody').on( 'click', '.edit_data', function () {
			        let id_ds = $(this).attr('id_ds');
			        s_Id_ds = id_ds;

			        let data = table.row( $(this).parents('tr') ).data();
			        $("#nama_ds").val(data["nama_ds"]);
			        $("#alamat").val(data["alamat"]);
			        $("#gender").val(data["gender"]);
					$("#no_hp").val(data["no_hp"]);
			        $("#email").val(data["email"]);

			        $('#modaltambah').modal({
					    focus: false
					});
			    });

			    $('#dataTable tbody').on( 'click', '.hapus_data', function () {
			        Swal.fire({
					  title: 'Are you sure?',
					  text: "You won't be able to revert this!",
					  type: 'warning',
					  animation: false,
					  customClass: 'animated tada',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Yes, delete it!'
					}).then((result) => {
					  if (result.value) {
					    let id_ds = $(this).attr('id_ds');
					    let jenis = "delete_data";

					    $.ajax({
					        type: 'POST',
					        url: "mahasiswa_action.php",
					        data: {id_ds:id_ds, jenis:jenis},
					        success: function(response) {
					        	if(response.code == 200){
									Swal.fire({
						              type: response.status,
						              title: 'Sukses',
						              text: response.message,
						              animation: false,
						              customClass: 'animated bounce'
						            }).then((result) => {
										$('#dataTable').DataTable().ajax.reload();
									});
								} else {
									Swal.fire({
									  type: response.status,
									  title: response.message,
									  text: '',
									})
								}
					        },error: function(response) {
				            	console.log(response.responseText);
				            }
					    });
					  }
					})
			    });
  			}

			function potongDeskripsi(string){
				let hasil = "";

				if (string=="" || string==null) {
					hasil = "";
				} else if(string.length > 15){
					hasil = string.substr(0, 15) + "...";
				} else {
					hasil = string;
				}
				return hasil;
			}

			$("#btnCari").click(function(){
				setDatatable();
			});

			$("#s_gender").change(function(){
				setDatatable();
			});

			$("#s_keyword").keyup(function(){
				setDatatable();
			});


			$("#btnTambah").click(function(){
				$('#modaltambah').modal({
				    focus: false
				});
			$("#btnTambah").click(function(){
				$('#modaltambah').modal({
				    focus: false
				});
				$('#form-data')[0].reset();

CKEDITOR.instances['nama_ds'].setData("");
s_Id_ds = "";
});
				$('#form-data')[0].reset();

				CKEDITOR.instances['alamat'].setData("");
				s_Id_ds = "";
			});

			$(".wajib").change(function(){
			    let nama_ds = $(this).attr('nama_ds');
			    $('#err_'+nama_ds).html("");
			});

			$("#btnSimpan").click(function(){
			    simpanData();
			});

			$('#form-data').keypress(function (e) {
			    if (e.which == 10) {
			        simpanData();
			    }
			});

		    function simpanData(){
	            let nama_ds = $('#nama_ds').val();
				let alamat= $('#alamat').val();
	            let gender = $('#gender').val();
	            let no_hp = $('#no_hp').val();
	            let email = $('#email').val();
	            let jenis = "edit_data";
				console.log(s_Id_ds)
            	if(s_Id_ds==""){
            		jenis = "edit_data";
            	}
	            if (nama_ds!="" && gender!=""  && alamat!=""  && no_hp!="" && email!="") {
	            	let formData = new FormData();
			        formData.append('id_ds',s_Id_ds);
			        formData.append('nama_ds', nama_ds);
			        formData.append('gender', gender);
			        formData.append('alamat', alamat);
			        formData.append('no_hp', no_hp);
			        formData.append('email', email);
			        formData.append('jenis', jenis);

					console.log(...formData)
			        $.ajax({
			            type: 'POST',
			            url: "mahasiswa_action.php",
			            data: formData,
			            cache: false,
		                processData: false,
		                contentType: false,
			            success: function(response) {
			            	if(response.code == 200){
								Swal.fire({
								  type: 'success',
								  title: 'Sukses',
								  text: 'Data Berhasil Disimpan',
								  animation: false,
		  						  customClass: 'animated fadeInDown'
								}).then((result) => {
									// $('#dataTable').DataTable().ajax.reload();
				                	$('#modaltambah').modal('hide');
								}).finally(() => {
									window.location.reload()
								});
							} else {
								Swal.fire({
								  type: "error",
								  title: "Error",
								  text: 'Telah terjadi kesalahan, coba lagi nanti',
								})
							}
			            },error: function(jqXHR, textStatus, error) {
			            	console.log(jqXHR, textStatus, error);
			            }
			        });
			    }
		    }
		});

	</script>


            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

    <script src="vendor/animsition/animsition.min.js"></script>

    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

</body>

</html>
<!-- end document-->
