!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../css/lv2.css">
    <link href="../assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="../assets/css/custom-styles.css" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!-- SweetAlert -->
    <link rel="stylesheet" type="text/css" href="../assets/css/sweetalert2.min.css">
	
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  
</head>
<body>
<div class="menu">
    <ul>
    <div class="logo"> <a href="indexlv2.php"> <img src="../foto/log.png" /></a></div>
    <li><a href="../logout.php">LOGOUT   <i class="fa fa-sign-out "> </i></a></li>      
	</ul>
	<div class="menu2">
    <ul>
		<li><a href="marketplace.php">Marketplace </i></a></li>   
		<li><a href="data-transaksi.php">Data Transaksi </i></a></li>
		<li><a href="dompetku.php">Dompetku </i></a></li>
        <li><a href="keranjang.php">Keranjang </i></a></li>
		<li><a href="#">Gudang Informasi </i></a></li>   
		<li><a href="#">Profilku </i></a></li>
		<li><a href="#">Panduan </i></a></li>
	</ul>
	</div>
</div>
<center>
	<div class="malasngoding-slider pt-lg-5">
		<div class="isi-slider">
			<img src="../foto/1.png" alt="Gambar 1">
		</div>
	</div>
	<div class=menu-user>
		<div class="isi-menu ">
			<table>
            <tr>
				<td><a href="marketplace.php"><button id="WA"><i class="fa fa-shopping-cart fa-2x"></i><br>Marketplace</button></a></td>
				<td><a href="data-transaksi.php"><button id="WA"><i class="fa fa-file-text fa-2x"></i><br>Data Transaksi</button></a></td>
				<td><a href="dompetku.php"><button id="WA"><i class="fa fa-money fa-2x"></i><br>Dompetku</button></a></td>
				<tr>
				<td><a href="#"><button id="WA"><i class="fa fa-lightbulb-o fa-2x"></i><br>Gudang Informasi</button></a></td>
				<td><a href="profile.php"><button id="WA"><i class="fa fa-user fa-2x"></i><br>Profilku</button></a></td>
				<td><a href="#"><button id="WA"><i class="fa fa-info-circle fa-2x"></i><br>Panduan</button></a></td>
				</tr>
			</table>
	</div>
</div>
</center>

<div class="container my-md-5 pb-lg-5">
	<div class="d-flex justify-content-center pb-lg-12"><p class="h1">Form Pembelian</p></div>
	<div class="d-flex row justify-content-center">
        <div class="col-lg-8 p-3 items">
      
            <div class="form-group">
                <label for="nama_pembeli">Nama Pembeli</label>
                <input type="text" class="form-control" id="nama_pembeli" aria-describedby="nama_pembeli" placeholder="Nama Pembeli"/>
            </div>
            <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="text" class="form-control" id="alamat" placeholder="Alamat">
            </div>
            <div class="form-group">
                <label for="no_hp">Nomor handphone</label>
                <input type="text" class="form-control" id="no_hp" aria-describedby="no_hp" placeholder="nomor handphone">
            </div>
            <div class="form-group">
                <label for="nama_pengirim">Nama pengirim</label>
                <input type="text" class="form-control" id="nama_pengirim" placeholder="nama pengirim">
            </div>
            <div class="form-group">
                <label for="harga_jual">Harga Jual ke Konsumen</label>
                <input type="number" class="form-control" id="harga_jual" placeholder="Harga jual">
            </div>
            <div class="form-group">
                <label for="no_hp_pengirim">Nomor handphone pengirim</label>
                <input type="text" class="form-control" id="no_hp_pengirim" aria-describedby="no_hp_pengirim" placeholder="nomor handphone pengirim">
            </div>
            
            <div class="form-group">
                <label for="jasa_pengiriman">Jasa pengiriman</label>
                <select class="form-control" id="jasa_pengiriman">
                <option value="kantor_pos">Kantor POS</option>
                <option value="ninja_express">Ninja Express</option>
                <option value="jnt">JNT</option>
                </select>
            </div>

            <button type="button" onclick="confirmation()" class="btn btn-primary">Submit</button>
		</div>
	</div>
</div>
    <script src="../assets/js/sweetalert2.min.js"></script>
	<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>
  <script>
	function getCookie(cName){
		let cookie 
		const name = cName + "="
		const cDecoded = decodeURIComponent(document.cookie)
		const cArr = cDecoded.split(';')

		cArr.forEach(val => {
			if(val.indexOf(name) === 0) cookie = val.substring(name.length)
		})
		return cookie
	}
 
    function confirmation(){
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Pastikan data yang diisi sudah benar!",
            type:"warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Beli Sekarang'
            }).then((result) => {
               confirmBuy()
        })
    }

	function getTotalHarga(id){
		return $.ajax({
			url: `../api/api_keranjang.php?action=get_grand_total&id_dropshipper=${id}`,
			type: "GET",
		});
	}
    
    function confirmBuy(){
        let id_ds = getCookie(" id_ds")
        const nama_pembeli = $("#nama_pembeli").val()
        const alamat = $("#alamat").val()
        const no_hp =  $("#no_hp").val()
        const nama_pengirim = $("#nama_pengirim").val()
        const no_hp_pengirim = $("#no_hp_pengirim").val()
        const jasa_pengiriman = document.getElementById("jasa_pengiriman")
        const get_grand_total = getTotalHarga(id_ds)
        const harga_jual = $("#harga_jual").val()
        
        let formData = new FormData()
        get_grand_total.done(function(data, textStatus, jqXHR){
            formData.set("grand_total", data.total)
            formData.set("nama_pembeli", nama_pembeli)
            formData.set("alamat", alamat)
            formData.set("no_hp", no_hp)
            formData.set("nama_pengirim", nama_pengirim)
            formData.set("no_hp_pengirim", no_hp_pengirim)
            formData.set("jasa_pengiriman", jasa_pengiriman.value)
            formData.set("harga_jual", harga_jual)

            $.ajax({
                url: `../api/api_transaksi.php?action=dropshipper_create_transaksi&id_dropshipper=${id_ds}`,
                type: "POST",
                processData: false,
                contentType: false,
                data: formData,
                success: function(response){
                    alert("Berhasil membeli barang")
                },
                error: function(jqXHR, textStatus, erro){
                    console.log(jqXHR)
                }
		    })
        })
    }
  </script>
</body>
</html>


