<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../css/lv2.css">
    <link href="../assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="../assets/css/custom-styles.css" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
	
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	
<div class="menu">
    <ul>
    <div class="logo"> <a href="indexlv2.php"> <img src="../foto/log.png" /></a></div>
    <li><a href="../logout.php">LOGOUT   <i class="fa fa-sign-out "> </i></a></li>      
	</ul>
	<div class="menu2">
    <ul>
		<li><a href="marketplace.php">Marketplace </i></a></li>   
		<li><a href="data-transaksi.php">Data Transaksi </i></a></li>
		<li><a href="dompetku.php">Dompetku </i></a></li>
		<li><a href="keranjang.php">Keranjang </i></a></li>
		<li><a href="#">Gudang Informasi </i></a></li>   
		<li><a href="#">Profilku </i></a></li>
		<li><a href="#">Panduan </i></a></li>
	</ul>
	</div>
</div>
<center>
	<div class="malasngoding-slider pt-lg-5">
		<div class="isi-slider">
			<img src="../foto/1.png" alt="Gambar 1">

		</div>
	</div>
	<div class=menu-user>
		<div class="isi-menu ">
			<table>
			<tr>
				<td><a href="marketplace.php"><button id="WA"><i class="fa fa-shopping-cart fa-2x"></i><br>Marketplace</button></a></td>
				<td><a href="data-transaksi.php"><button id="WA"><i class="fa fa-file-text fa-2x"></i><br>Data Transaksi</button></a></td>
				<td><a href="dompetku.php"><button id="WA"><i class="fa fa-money fa-2x"></i><br>Dompetku</button></a></td>
				<tr>
				<td><a href="#"><button id="WA"><i class="fa fa-lightbulb-o fa-2x"></i><br>Gudang Informasi</button></a></td>
				<td><a href="profile.php"><button id="WA"><i class="fa fa-user fa-2x"></i><br>Profilku</button></a></td>
				<td><a href="#"><button id="WA"><i class="fa fa-info-circle fa-2x"></i><br>Panduan</button></a></td>
				</tr>
			</table>
	</div>
</div>
</center>

<div class="container my-md-5 pb-lg-5">
	<div class="d-flex justify-content-center pb-lg-5"><p class="h1">Produk Knalpot</p></div>
	<div class="d-flex justify-content-center row products">
	</div>
</div>
	<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>
  <script>
	function getCookie(cName){
		let cookie 
		const name = cName + "="
		const cDecoded = decodeURIComponent(document.cookie)
		const cArr = cDecoded.split(';')

		cArr.forEach(val => {
			if(val.indexOf(name) === 0) cookie = val.substring(name.length)
		})
		console.log(cookie)
		return cookie
	}
	
	function getAllProduct(){
		let products = ''
		var data = ''

		$.ajax({
			url: `../api/api_produk.php?action=get_all_product`,
			type: "GET",
			dataType: "json",
			success: function(response){
				$("card:first").hide()
				data = response.data
				$.each(data, function(idx, value){
					products =  `
					<div class="col-lg-3 pb-lg-3">
						<div class="card">
							<img src="${value.foto_produk}" class="card-img-top" alt="${value.nama_produk}">
							<div class="card-body">
								<input type="hidden"  id="${value.id_produk}-id_produk" name="id_produk">
								<h5 id="${value.id_produk}-nama_produk" name="nama_produk" class="card-title">${value.nama_produk}</h5>
								<h5 id="${value.id_produk}-berat" name="berat" class="card-title">${value.berat}</h5>
								<h4 id="${value.id_produk}-harga_jual" name="harga_jual" class="card-title">${value.harga_pokok}</h4>
								<p class="card-text" id="${value.id_produk}-deskripsi" name="deskripsi">${value.deskripsi}</p>
							</div>
							<div class="card-footer">
								<input type="number" id="${value.id_produk}-jumlah" name="jumlah" required></input>
								<button onclick="addToCart(${value.id_produk})" class="btn add-item btn-primary">Beli</button>
							</div>
						</div>
					</div>
					`
					$(".products").append(products)
				})
			},
			error: function(jqXHR, textStatus, erro){
				console.log(jqXHR)
			}

		})
		
	}
	
	getAllProduct()
	
	function addToCart(id){
		const harga = document.getElementById(id + "-harga_jual").innerHTML;
		const nama_produk = document.getElementById(id + "-nama_produk").innerHTML;
		const deskripsi = document.getElementById(id + "-deskripsi").innerHTML;
		const berat = document.getElementById(id + "-berat").innerHTML;
		const jumlah = $(`#${id}-jumlah`).val()
		const id_dropshipper = getCookie(" id_ds")
		const harga_satuan = document.getElementById(id + "-harga_jual").innerHTML;
		

		let formData = new FormData()

		if(jumlah === '' || jumlah <= 0){
			alert('jumlah tidak boleh kosong')
			return
		}

        formData.set("id_barang", id)
		formData.set("id_dropshipper", id_dropshipper)
		formData.set("nama", nama_produk)
		formData.set("berat", berat)
		formData.set("deskripsi", deskripsi)
		formData.set("jumlah", jumlah)	
		formData.set("harga_satuan", harga_satuan)

		$.ajax({
			url: `../api/api_keranjang.php?action=add_to_cart`,
			type: "POST",
			processData: false,
			contentType: false,
			data: formData,
			success: function(response){
				alert("berhasil menambahkan barang")
			},
			error: function(jqXHR, textStatus, erro){
				console.log(jqXHR)
			}

		})
	}
	
  </script>
</body>
</html>