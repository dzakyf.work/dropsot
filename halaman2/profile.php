<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../css/lv2.css">
    <!-- FontAwesome Styles-->
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="../assets/css/custom-styles.css" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
	 <!-- SweetAlert -->
    <link rel="stylesheet" type="text/css" href="../assets/css/sweetalert2.min.css">
	
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	
<div class="menu">
    <ul>
    <div class="logo"> <a href="indexlv2.php"> <img src="../foto/log.png" /></a></div>
    <li><a href="../logout.php">LOGOUT   <i class="fa fa-sign-out "> </i></a></li>      
	</ul>
	<div class="menu2">
    <ul>
		<li><a href="marketplace.php">Marketplace </i></a></li>   
		<li><a href="data-transaksi.php">Data Transaksi </i></a></li>
		<li><a href="dompetku.php">Dompetku </i></a></li>
		<li><a href="keranjang.php">Keranjang </i></a></li>
		<li><a href="#">Gudang Informasi </i></a></li>   
		<li><a href="profile.php">Profilku </i></a></li>
		<li><a href="#">Panduan </i></a></li>
	</ul>
	</div>
</div>
<center>
	<div class="malasngoding-slider pt-lg-5">
		<div class="isi-slider">
			<img src="../foto/1.png" alt="Gambar 1">
		</div>
	</div>
	<div class=menu-user>
		<div class="isi-menu">
			<table>
				<tr>
				<td><a href="marketplace.php"><button id="WA"><i class="fa fa-shopping-cart fa-2x"></i><br>Marketplace</button></a></td>
				<td><a href="data-transaksi.php"><button id="WA"><i class="fa fa-file-text fa-2x"></i><br>Data Transaksi</button></a></td>
				<td><a href="dompetku.php"><button id="WA"><i class="fa fa-money fa-2x"></i><br>Dompetku</button></a></td>
				<tr>
				<td><a href="#"><button id="WA"><i class="fa fa-lightbulb-o fa-2x"></i><br>Gudang Informasi</button></a></td>
				<td><a href="profile.php"><button id="WA"><i class="fa fa-user fa-2x"></i><br>Profilku</button></a></td>
				<td><a href="#"><button id="WA"><i class="fa fa-info-circle fa-2x"></i><br>Panduan</button></a></td>
				</tr>
			</table>
	</div>
</div>
</center>
    <div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail Transaksi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                    
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>

<div class="container my-md-5 pb-lg-5">
	<div class="d-flex justify-content-center pb-lg-5"><p class="h1">Profil</p></div>
        <div class="card bg-light mb-3" >
            <div class="card-body">
                <h5 class="card-title">Profil Dropshipper</h5>
                <div class="dropdown-divider"></div>
                    <div class="row">
                        <div class="col-6">
                            <div class="row">
                                <div class="col-sm-6"><p><strong>Nama Dropshipper</strong></p></div>
                                <div class="col-6 nama-dropshipper"></div>
                            </div>
                            <div class="row">
                                <div class="col-6"><p><strong>Gender</strong></p></div>
                                <div class="col-6 gender"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6"><p><strong>Alamat</strong></p></div>
                                <div class="col-6 alamat"></div>
                            </div>
                            <div class="row">
                                <div class="col-6"><p><strong>Nomor Handphone</strong></p></div>
                                <div class="col-6 no-hp"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6"><p><strong>Email</strong></p></div>
                                <div class="col-6 email"></div>
                            </div>
                            <div class="row">
                                <div class="col-6"><p><strong>Username / Name</strong></p></div>
                                <div class="col-6 username"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="row">
                                <div class="col-sm-6"><p><strong>Bank</strong></p></div>
                                <div class="col-6 bank"></div>
                            </div>
                            <div class="row">
                                <div class="col-6"><p><strong>Nomor Rekening</strong></p></div>
                                <div class="col-6 no-rekening"></div>
                            </div>
                            <div class="row">
                                <div class="col-6"><p><strong>Nama Rekening</strong></p></div>
                                <div class="col-6 nama-rekening"></div>
                            </div>
                        <div>
                        
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
<script src="../assets/js/sweetalert2.min.js"></script>
<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script>
        function getCookie(cName){
            let cookie 
            const name = cName + "="
            const cDecoded = decodeURIComponent(document.cookie)
            const cArr = cDecoded.split(';')

            cArr.forEach(val => {
                if(val.indexOf(name) === 0) cookie = val.substring(name.length)
            })
            return cookie
        }

        function getDetailBank(id_dropshipper){
            return $.ajax({
                'url' : `../api/api_pencairan.php?action=get_bank_detail&id_dropshipper=${id_dropshipper}`,
                'type' : "GET"
            })
        }


        function getDropshipperById(){
            id_dropshipper = getCookie(" id_ds")
            $.ajax({
                'url': `../api/api_dropshipper.php?action=get_dropshipper_by_id&id_dropshipper=${id_dropshipper}`,
                'type' : 'GET',
                success: function(data, textStatus, jqXHR){
                    details = data.data[0]
                    getDetailBank(id_dropshipper).done(function(data, textStatus, jqXHR){
                        bank = data.data
                        console.log(data)
                        $(".bank").append(`<p>${bank.bank}</p>`)
                        $(".no-rekening").append(`<p>${bank.no_rekening}</p>`)
                        $(".nama-rekening").append(`<p>${bank.nama_rekening}</p>`)
                    })
                    $(".nama-dropshipper").append(`<p>${details.nama_ds}</p>`)
                    $(".gender").append(`<p>${details.gender}</p>`)
                    $(".alamat").append(`<p>${details.alamat}</p>`)
                    $(".no-hp").append(`<p>${details.no_hp}</p>`)
                    $(".email").append(`<p>${details.email}</p>`)
                    $(".username").append(`<p>${details.username}</p>`)
                   

                }
            })
        }
        getDropshipperById()
  </script>
</body>
</html>