<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../css/lv2.css">
    <link href="../assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="../assets/css/custom-styles.css" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
	 <!-- SweetAlert -->
    <link rel="stylesheet" type="text/css" href="../assets/css/sweetalert2.min.css">
	
	
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  
</head>
<body>
	
<div class="menu">
    <ul>
    <div class="logo"> <a href="indexlv2.php"> <img src="../foto/log.png" /></a></div>
    <li><a href="../logout.php">LOGOUT   <i class="fa fa-sign-out "> </i></a></li>      
	</ul>
	<div class="menu2">
    <ul>
		<li><a href="marketplace.php">Marketplace </i></a></li>   
		<li><a href="data-transaksi.php">Data Transaksi </i></a></li>
		<li><a href="dompetku.php">Dompetku </i></a></li>
		<li><a href="keranjang.php">Keranjang </i></a></li>
		<li><a href="#">Gudang Informasi </i></a></li>   
		<li><a href="#">Profilku </i></a></li>
		<li><a href="#">Panduan </i></a></li>
	</ul>
	</div>
</div>
<center>
	<div class="malasngoding-slider pt-lg-5">
		<div class="isi-slider">
			<img src="../foto/1.png" alt="Gambar 1">

		</div>
	</div>
	<div class=menu-user>
		<div class="isi-menu ">
			<table>
			<tr>
				<td><a href="marketplace.php"><button id="WA"><i class="fa fa-shopping-cart fa-2x"></i><br>Marketplace</button></a></td>
				<td><a href="data-transaksi.php"><button id="WA"><i class="fa fa-file-text fa-2x"></i><br>Data Transaksi</button></a></td>
				<td><a href="dompetku.php"><button id="WA"><i class="fa fa-money fa-2x"></i><br>Dompetku</button></a></td>
				<tr>
				<td><a href="#"><button id="WA"><i class="fa fa-lightbulb-o fa-2x"></i><br>Gudang Informasi</button></a></td>
				<td><a href="profile.php"><button id="WA"><i class="fa fa-user fa-2x"></i><br>Profilku</button></a></td>
				<td><a href="#"><button id="WA"><i class="fa fa-info-circle fa-2x"></i><br>Panduan</button></a></td>
				</tr>
			</table>
	</div>
</div>
</center>

<div class="container my-md-5 pb-lg-5">
	<div class="d-flex justify-content-center pb-lg-5"><p class="h1">Dompetku</p></div>
    <div class="card">
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Transaksi pada tanggal</th>
                    <th scope="col">Harga Jual ke Konsumen</th>
                    <th scope="col">Harga Beli</th>
                    <th scope="col">Keuntungan</th>

                    </tr>
                </thead>
                <tbody class="data-transaksi">
                </tbody>
            </table>
        </div>
    </div>
	</div>
</div>
<script src="../assets/js/sweetalert2.min.js"></script>
	<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>
  <script>
	function getCookie(cName){
		let cookie 
		const name = cName + "="
		const cDecoded = decodeURIComponent(document.cookie)
		const cArr = cDecoded.split(';')

		cArr.forEach(val => {
			if(val.indexOf(name) === 0) cookie = val.substring(name.length)
		})
		console.log(cookie)
		return cookie
	}
	function getTransactionSuccess(){
		let status_pencairan = 'BELUM_DICAIRKAN'
		let status_transaksi = 'SELESAI_DIPROSES'
		let id_dropshipper = getCookie(" id_ds")
		$.ajax({
			url: `../api/api_transaksi.php?action=get_transaksi_by_status_pencairan&status_transaksi=${status_transaksi}&status_pencairan=${status_pencairan}&id_dropshipper=${id_dropshipper}`,
			type: "GET",
			dataType: "json",
			async : false,
			success: function(response){
				datas = response.data
				$.each(datas, function(idx, value){
				row = 
				`<tr id="row-${idx}">
						<th scope="row">${idx+1}</th>
						<td>${value.tanggal_transaksi}</td>
						<td>${value.harga_jual}</td>
						<td>${value.grand_total}</td>
						<td>${value.harga_jual - value.grand_total}</td>
						<td><button class="btn btn-primary" onclick="requestPencairan(${value.id_transaksi})">Cairkan</button></td>					
                </tr>`
				$(".data-transaksi").append(row)
				})
			},
			error: function(jqXHR, textStatus, erro){
				console.log(jqXHR)
			}
		})
	}
	
	getTransactionSuccess()

	function requestPencairan(id){
		$.ajax({
			url: `../api/api_pencairan.php?action=dropshipper_request_pencairan&id_transaksi=${id}`,
			type: "POST",
			success: function(response){
				Swal.fire({
					type: 'success',
					title: 'Sukses',
					text: 'Berhasil mengajukan pencairan',
					timer: 3000,
					timerProgressBar: true,
					// animation: false,
					customClass: 'animated fadeInDown'
				}).finally(() => {
					window.location.reload()
				});
			},
			error: function(jqXHR, textStatus, error){
				console.log(jqXHR)
			}
		})
	}
  </script>
</body>
</html>