<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../css/lv2.css">
    <link href="../assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="../assets/css/custom-styles.css" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
	
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  
</head>
<body>
<div class="menu">
    <ul>
    <div class="logo"> <a href="indexlv2.php"> <img src="../foto/log.png" /></a></div>
    <li><a href="../logout.php">LOGOUT   <i class="fa fa-sign-out "> </i></a></li>      
	</ul>
	<div class="menu2">
    <ul>
		<li><a href="marketplace.php">Marketplace </i></a></li>   
		<li><a href="data-transaksi.php">Data Transaksi </i></a></li>
		<li><a href="dompetku.php">Dompetku </i></a></li>
		<li><a href="keranjang.php">Keranjang </i></a></li>
		<li><a href="#">Gudang Informasi </i></a></li>   
		<li><a href="#">Profilku </i></a></li>
		<li><a href="#">Panduan </i></a></li>
	</ul>
	</div>
</div>
<center>
	<div class="malasngoding-slider pt-lg-5">
		<div class="isi-slider">
			<img src="../foto/1.png" alt="Gambar 1">
		</div>
	</div>
	<div class=menu-user>
		<div class="isi-menu ">
			<table>
			<tr>
				<td><a href="marketplace.php"><button id="WA"><i class="fa fa-shopping-cart fa-2x"></i><br>Marketplace</button></a></td>
				<td><a href="data-transaksi.php"><button id="WA"><i class="fa fa-file-text fa-2x"></i><br>Data Transaksi</button></a></td>
				<td><a href="dompetku.php"><button id="WA"><i class="fa fa-money fa-2x"></i><br>Dompetku</button></a></td>
				<tr>
				<td><a href="#"><button id="WA"><i class="fa fa-lightbulb-o fa-2x"></i><br>Gudang Informasi</button></a></td>
				<td><a href="profile.php"><button id="WA"><i class="fa fa-user fa-2x"></i><br>Profilku</button></a></td>
				<td><a href="#"><button id="WA"><i class="fa fa-info-circle fa-2x"></i><br>Panduan</button></a></td>
				</tr>
			</table>
	</div>
</div>
</center>

<div class="container my-md-5 pb-lg-5">
	<div class="d-flex justify-content-center pb-lg-5"><p class="h1">Keranjang</p></div>
	<div class="d-flex row">
		<div class="col-lg-8 p-3 items">
		</div>
		<div class="col-lg-3 p-3">
			<div class="card">
				<div class="card-body">
					<div id="grand_total"></div>
					<a href="./form-beli.php" class="btn btn-primary">Beli Sekarang</a>
				</div>
			</div>
		</div>
	</div>
</div>
	<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>
  <script>
	function getCookie(cName){
		let cookie 
		const name = cName + "="
		const cDecoded = decodeURIComponent(document.cookie)
		const cArr = cDecoded.split(';')

		cArr.forEach(val => {
			if(val.indexOf(name) === 0) cookie = val.substring(name.length)
		})
		return cookie
	}

	function getTotalHarga(){
		let id_ds = getCookie(" id_ds")
		$.ajax({
			url: `../api/api_keranjang.php?action=get_grand_total&id_dropshipper=${id_ds}`,
			type: "GET",
			success: function(response, textStatus, jqXHR){
				$("#grand_total").append(`
					<p id="grand_total">Total Harga: ${response.total}</p>
				`)
			},
			error: function(jqXHR, textStatus, error){
				alert(jqXHR)
			}
		})
	}

	function getDetailItem(id, result){
		return $.ajax({
			url: `../api/api_produk.php?action=get_product_by_id&id=${id}`,
			type: "GET",
		})
	}

	function deleteCartItem(id_item){
		var request = $.ajax({
			url: `../api/api_keranjang.php?action=delete_item&id_item=${id_item}`,
			type: "DELETE",
			success: function(response, textStatus, jqXHR){
				alert('berhasil menghapus item')
			},
			error: function(jqXHR, textStatus, error){
				alert(jqXHR)
			}
		})

		request.then(() => {
			window.location.reload()
		})
	}

	function modifyQuantity(id){
		let qty = $(`.${id}-jumlah`).val()
		let formData = new FormData()
		formData.set("jumlah", parseInt(qty))
		$.ajax({
			url: `../api/api_keranjang.php?action=modify_item_quantity&id_item=${id}`,
			type: "POST",
			data: formData,
			processData: false,
			contentType: false,
			success: function(response, textStatus, jqXHR){
				console.log('Berhasil mengubah keranjang')
				$(`.${id}-jumlah`).val(qty)
				window.location.reload()
			},
			error: function(jqXHR, textStatus, error){
				console.log(jqXHR)
				return
			}
		})
	}

	
	function getAllItem(){
		let products = ''
		var id_ds = getCookie(" id_ds")
		getTotalHarga()
		$.ajax({
			url: `../api/api_keranjang.php?action=get_cart_item&id_dropshipper=${id_ds}`,
			type: "GET",
			dataType: "json",
			async : false,
			success: function(response){
				$("card:first").hide()
				$.each(response.data, function(idx, value){
					var promise = getDetailItem(parseInt(value.id_barang))
					promise.done(function(data, textStatus, jqXHR){
						detailItem = data.data[0]
						products =  
						`
							<div class="card m-2">
								<div class="card-body">
								<img src="${detailItem.foto_produk}" class="rounded float-left pr-lg-3" width="auto" height="60px" alt="${detailItem.nama_produk}">
									<h5 class="card-title">${detailItem.nama_produk}</h5>
									<h4 class="card-title">Rp. ${detailItem.harga_pokok}</h4>
									<p class="card-text">${detailItem.deskripsi}</p>
								</div>
								<div class="card-footer">
									<div class="d-flex ">
										<div class="p-2 bd-highlight">Jumlah :</div>
										<div class="p-2 bd-highlight input-jumlah">
											<input id="${value.id_transaksi_barang}-jumlah" class="${value.id_transaksi_barang}-jumlah" onchange="modifyQuantity(${value.id_transaksi_barang})" type="number" value="${value.jumlah}" min="0""></input>
										</div>
										<div class="p-2 float-right"><button type="button" class="btn btn-danger" onclick="deleteCartItem(parseInt(${value.id_transaksi_barang}))">Hapus</button></div>
									</div>
								</div>
							</div>
						`
					$(".items").append(products)
					})
				})
			},
			error: function(jqXHR, textStatus, erro){
				console.log(jqXHR)
			}
		})
	}
	
	getAllItem()
  </script>
</body>
</html>