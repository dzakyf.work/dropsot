<?php 
    define('__ROOT__', dirname(dirname(__FILE__)));
    require_once __ROOT__.'/database/koneksi.php';

    if(function_exists($_GET['action'])) {
         $_GET['action']();
    }   

    function cart_sum($carry, $item){
        $carry += $item;
        return $carry;

    }

    function get_grand_total(){
        global $connect;

        $grand_total_arr = array();

        $id_ds = $_GET['id_dropshipper'];

        $query = mysqli_query($connect, "SELECT total_harga FROM transaksi_barang WHERE id_dropshipper=" .$id_ds. " AND id_transaksi IS NULL");

        while($row = mysqli_fetch_row($query)){
            $counter = 0;
            array_push($grand_total_arr, (int)$row[$counter]);
            $counter += 1;
        }


        if($query){
            $response = array(
                'status' => 200,
                'message' => "success",
                'total' =>  array_reduce($grand_total_arr, "cart_sum")
            );
        }else{
            die('Error: '. mysqli_error($query));
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    
    }

    function add_to_cart(){
        global $connect;

        $id_ds = $_POST['id_dropshipper'];
        $id_barang = $_POST['id_barang'];
        $nama = $_POST['nama'];
        $berat = $_POST['berat'];
        $jumlah = $_POST['jumlah'];
        $deskripsi = $_POST['deskripsi'];
        $harga_satuan = $_POST['harga_satuan'];
        $total_harga = $harga_satuan * $jumlah;

        $query = mysqli_query($connect, "INSERT INTO transaksi_barang(
            id_dropshipper,
            id_barang,
            nama,
            berat,
            jumlah,
            deskripsi,
            harga_satuan,
            total_harga
        )
        VALUES(
            $id_ds,
            $id_barang,
            '$nama',
            $berat,
            $jumlah,
            '$deskripsi',
            $harga_satuan,
            $total_harga
        )");

        if($query){
            $response = array(
                'status' => 200,
                'message' => "success",
            );
        }else{
            die('Error: '. mysqli_error($query));
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }
    

   

    //get all item by id_dropshipper
    function get_cart_item(){
        global $connect;

        $data = array();
        $id_ds = $_GET['id_dropshipper'];
        $query = mysqli_query($connect, "SELECT * FROM transaksi_barang WHERE id_dropshipper =". $id_ds ." AND id_transaksi IS NULL");
        while($row = mysqli_fetch_object($query)){
            $data[] = $row;
        }

        if($query){
            $response = array(
                'status' => 200,
                'message' => "success",
                'data' => $data
            );
        }else{
            die('Error: '. mysqli_error($query));
        }

        header('Content-Type: application/json');
        echo json_encode($response);

    }

    function calculate_total_harga($id_item, $quantity){
        global $connect;
        $get_current_harga_satuan = mysqli_prepare($connect, "SELECT harga_satuan FROM transaksi_barang WHERE id_transaksi_barang = ?");
        mysqli_stmt_bind_param($get_current_harga_satuan, 'i', $id_item);
        mysqli_stmt_execute($get_current_harga_satuan);
        $result_current_total_harga = mysqli_stmt_get_result($get_current_harga_satuan);


        $current_total_harga = '';
        while($row = mysqli_fetch_object($result_current_total_harga)){
            $current_total_harga = $row->harga_satuan;
        }

       return (int)$current_total_harga * $quantity;

    }
     
    function modify_item_quantity(){
        global $connect;

        $id_item = $_GET['id_item'];
        $quantity = $_POST['jumlah'];
        
        $update_jumlah = mysqli_prepare($connect,"UPDATE transaksi_barang SET
        jumlah = ?
        WHERE id_transaksi_barang = ?");

        mysqli_stmt_bind_param($update_jumlah, 'ii', $quantity,$id_item);
        mysqli_stmt_execute($update_jumlah);

        $curr_total_harga = calculate_total_harga($id_item, $quantity);

        $update_total_harga= mysqli_prepare($connect, "UPDATE transaksi_barang SET
        total_harga = ?
        WHERE id_transaksi_barang = ?");


        mysqli_stmt_bind_param($update_total_harga, 'ii', $curr_total_harga, $id_item);
        mysqli_stmt_execute($update_total_harga);
        // mysqli_multi_query($connect, $query);

        // $query = mysqli_prepare($connect, "UPDATE transaksi_barang SET
        // jumlah = $quantity
        // WHERE id_transaksi_barang=".$id_item);

        // $curr_total_harga = calculate_total_harga($id_item, $quantity);
      
        // $update_total_harga = mysqli_prepare($connect, "UPDATE transaksi_barang SET
        // total_harga = $curr_total_harga
        // WHERE id_transaksi_barang = ?");


        if ($update_jumlah && $update_total_harga){
             $response = array(
                'status' => 200,
                'message' => "success",
            );
        }else{
            die('Error: ');
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    

    function delete_item(){
        global $connect; 

        $id_item = $_GET['id_item'];
        $query = mysqli_query($connect, "DELETE FROM transaksi_barang WHERE id_transaksi_barang=".$id_item);

         if ($query){
            $response = array(
                'status' => 200,
                'message' => "data berhasil dihapus.",
            );
        }else{
            die('Error: '. mysqli_error($query));
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }
    

?>