<?php 
    define('__ROOT__', dirname(dirname(__FILE__)));
    require_once __ROOT__.'/database/koneksi.php';

    if(function_exists($_GET['action'])) {
         $_GET['action']();
    }   

    function get_bank_detail(){
        global $connect;
        $id_dropshipper = $_GET['id_dropshipper'];
        $get_bank_detail = mysqli_prepare($connect,
        "SELECT no_rek, nama_rek, bank FROM dropshipper WHERE id_ds = ?"
        );
        mysqli_stmt_bind_param($get_bank_detail, 'i', $id_dropshipper);
        mysqli_stmt_execute($get_bank_detail);
        $result = mysqli_stmt_get_result($get_bank_detail);

        while($row = mysqli_fetch_array($result)){
            $bank_detail = array();
            $bank_detail['bank'] = $row["bank"];
            $bank_detail["no_rekening"] = $row["no_rek"];
            $bank_detail["nama_rekening"] =$row["nama_rek"];
        }
         if($result){
             $response = array(
                'status' => 200,
                'message' => "success",
                'data' => $bank_detail
            );
        }else{
            die('Error: '. mysqli_error($confirm_pesanan));
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function admin_setujui_pencairan(){
        global $connect;
        $id_transaksi = $_GET['id_transaksi'];
        $status_pencairan = "DICAIRKAN";
        $bukti_transfer = $_FILES['foto_bukti_transfer'];
        $foto_name = "bkt_trans_".$bukti_transfer['name'];
        $protocol = $_SERVER['PROTOCOL'] = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https' : 'http';
        $path = $protocol . "://" . $_SERVER['SERVER_NAME']."/dropshot"."/foto/upload/".$foto_name;
        move_uploaded_file($bukti_transfer['tmp_name'], __ROOT__."/foto/upload/". $foto_name);


        $setujui_pencairan_query = mysqli_prepare($connect, "UPDATE transaksi SET
        status_pencairan = ?,
        bukti_transfer_path = ?
        WHERE id_transaksi = ?
        ");
        mysqli_stmt_bind_param($setujui_pencairan_query, 'ssi', $status_pencairan, $path, $id_transaksi);
        mysqli_stmt_execute($setujui_pencairan_query);
        $result = mysqli_stmt_get_result($setujui_pencairan_query);

        if($result){
             $response = array(
                'status' => 200,
                'message' => "success",
            );
        }else{
            die('Error: '. mysqli_error($result));
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }


    function dropshipper_request_pencairan(){
        global $connect;

        $id_transaksi = $_GET['id_transaksi'];
        $status_pencairan = 'MENUNGGU_KONFIRMASI';

        $update_status_pencairan = mysqli_prepare($connect, "UPDATE transaksi SET
        status_pencairan = ?
        WHERE id_transaksi= ?" );
        mysqli_stmt_bind_param($update_status_pencairan, 'si', $status_pencairan, $id_transaksi);
        $result = mysqli_stmt_execute($update_status_pencairan);

        if($result){
             $response = array(
                'status' => 200,
                'message' => "success",
            );
        }else{
            die('Error: '. mysqli_error($confirm_pesanan));
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }
?>