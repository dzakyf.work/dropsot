<?php 
    define('__ROOT__', dirname(dirname(__FILE__)));
    require_once __ROOT__.'/database/koneksi.php';

    if(function_exists($_GET['action'])) {
         $_GET['action']();
    }   

    function dropshipper_create_transaksi(){
        global $connect;

        $id_ds = $_GET['id_dropshipper'];
        $status_transaksi = 'BELUM_DIPROSES';
        $nama_pembeli = $_POST['nama_pembeli'];
        $alamat = $_POST['alamat'];
        $no_hp = $_POST['no_hp']; 
        $nama_pengirim = $_POST['nama_pengirim'];
        $no_hp_pengirim = $_POST['no_hp_pengirim'];
        $jasa_pengiriman = $_POST['jasa_pengiriman'];
        $grand_total = $_POST['grand_total'];
        $harga_jual = $_POST['harga_jual'];

        // $query = mysqli_prepare($connect, 
        //  "INSERT INTO transaksi(
        // status_transaksi,
        // nama_pembeli,
        // alamat,
        // no_hp,
        // nama_pengirim,
        // no_hp_pengirim,
        // jasa_pengiriman,
        // grand_total,
        // harga_jual,
        // id_dropshipper
        // )
        // VALUES(
        // ?,?,?,?,?,?,?,?,?,?);");
        // mysqli_stmt_bind_param($query, "sssssssiii",
        // $status_transaksi,
        // $nama_pembeli,
        // $alamat,
        // $no_hp,
        // $nama_pengirim,
        // $no_hp_pengirim,
        // $jasa_pengiriman,
        // $grand_total,
        // $harga_jual,
        // $id_dropshipper
        // );
        // mysqli_stmt_execute($query);

        $query = mysqli_query($connect, 
        "INSERT INTO transaksi(
        status_transaksi,
        nama_pembeli,
        alamat,
        no_hp,
        nama_pengirim,
        no_hp_pengirim,
        jasa_pengiriman,
        grand_total,
        harga_jual,
        id_dropshipper
        )
        VALUES(
        '$status_transaksi',
        '$nama_pembeli',
        '$alamat',
        '$no_hp',
        '$nama_pengirim',
        '$no_hp_pengirim',
        '$jasa_pengiriman',
        $grand_total,
        $harga_jual,
        $id_ds
        );");

        $query_insert_id_transaksi = mysqli_query($connect, 
        "UPDATE transaksi_barang
        SET id_transaksi = '".mysqli_insert_id($connect)."'
        WHERE id_dropshipper =". $id_ds. " AND id_transaksi IS NULL");

        if($query && $query_insert_id_transaksi){
            $response = array(
                'status' => 200,
                'message' => "success",
            );
        }else{
            die('Error: '. mysqli_error($result));
        }
        
         header('Content-Type: application/json');
         echo json_encode($response);
    }

    function admin_update_transaksi_diproses(){
        global $connect;

        $id_transaksi = $_GET['id_transaksi'];

        $confirm_pesanan = mysqli_query($connect, "UPDATE transaksi SET
        status_transaksi = 'SEDANG_DIPROSES'
        WHERE id_transaksi=" .$id_transaksi);

        if($confirm_pesanan){
             $response = array(
                'status' => 200,
                'message' => "success",
            );
        }else{
            die('Error: '. mysqli_error($confirm_pesanan));
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function admin_update_transaksi_dikirim(){
        global $connect;

        $id_transaksi = $_POST['id_transaksi'];
        $foto = $_FILES['foto_barang_dikirim'];
        $no_resi = $_POST['no_resi'];
        $foto_name = "brg_dkrm_".$_FILES['foto_barang_dikirim']['name'];
        $protocol = $_SERVER['PROTOCOL'] = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https' : 'http';
        $path = $protocol . "://" . $_SERVER['SERVER_NAME']."/dropshot"."/foto/upload/".$foto_name;
        move_uploaded_file($foto['tmp_name'], __ROOT__."/foto/upload/". $foto_name);
       
        $query = mysqli_query($connect, "UPDATE transaksi SET
        status_transaksi = 'DIKIRIM',
        photo_dikirim_path = '".$path."',
        no_resi = '".$no_resi."'
        WHERE id_transaksi=" .$id_transaksi);

        if ($query){
             $response = array(
                'status' => 200,
                'message' => "success",
            );
        }else{
            die('Error: '. mysqli_error($result));
        }

        header('Content-Type: application/json');
        echo json_encode($response);

    }

    function admin_update_transaksi_selesai(){
        global $connect;

        $id_transaksi = $_GET['id_transaksi'];
        $status_transaksi = "SELESAI_DIPROSES";

        $query = mysqli_query($connect, "UPDATE transaksi SET
        status_transaksi = '".$status_transaksi."'
        WHERE id_transaksi=".$id_transaksi);

        if ($query){
             $response = array(
                'status' => 200,
                'message' => "success",
            );
        }else{
            die('Error: '. mysqli_error($result));
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function get_all_transaksi(){
        global $connect;
        $data = array();

        $query = mysqli_query($connect, "SELECT * FROM transaksi");

        while($row = mysqli_fetch_object($query))
        {
            $data[] =$row;
        }

        if($query){
            $response = array(
                'status' => 200,
                'message' => "success",
                'data' => $data,
            );
        }else{
            die('Error: '.mysqli_error($query));
        }


        header('Content-Type: application/json');
        echo json_encode($response);

    }

    function admin_get_transaksi_by_status_pencairan(){
        global $connect;
        $status_pencairan = $_GET['status_pencairan'];
        $status_transaksi = "SELESAI_DIPROSES";
        $list_transaksi = array();
        $query = mysqli_prepare($connect, 
        "SELECT * FROM transaksi WHERE status_pencairan = ? AND status_transaksi = ?");
        mysqli_stmt_bind_param($query, 'ss', $status_pencairan, $status_transaksi);
        mysqli_stmt_execute($query);
        $result = mysqli_stmt_get_result($query);

        while($row = mysqli_fetch_array($result))
        {
            $data = array();
            $data["id_transaksi"] = (int)$row["id_transaksi"];
            $data["status_transaksi"] =$row["status_transaksi"]; 
             $data["status_pencairan"] =$row["status_pencairan"]; 
            $data["nama_pembeli"]=$row["nama_pembeli"];
            $data["alamat"]=$row["alamat"];
            $data["no_hp"]=$row["no_hp"];
            $data["nama_pengirim"]=$row["nama_pengirim"];
            $data["no_hp_pengirim"]=$row["no_hp_pengirim"];
            $data["jasa_pengiriman"]=$row["jasa_pengiriman"];
            $data["grand_total"]= (int)$row["grand_total"];
            $data["harga_jual"]= (int)$row["harga_jual"];
            $data["photo_dikirim_path"]=$row["photo_dikirim_path"];
            $data["no_resi"]=$row["no_resi"];
            $data["tanggal_transaksi"]=$row["created_at"];
            $data["id_dropshipper"] =$row["id_dropshipper"];
            $data["pesanan"] = array();
            
            $get_list_barang = mysqli_query($connect, "SELECT * FROM transaksi_barang WHERE id_transaksi=" .$row["id_transaksi"]);

            while($pesanan = mysqli_fetch_assoc($get_list_barang)){
                $data["pesanan"][] = array(
                    "id_barang" => (int)$pesanan["id_barang"],
                    "nama" => $pesanan["nama"],
                    "berat" => (int)$pesanan["berat"],
                    "jumlah" => (int)$pesanan["jumlah"],
                    "deskripsi" => $pesanan["deskripsi"],
                    "total_harga" => (int)$pesanan["total_harga"],
                    "harga_satuan" => (int)$pesanan["harga_satuan"],
                );
            }
            array_push($list_transaksi, $data);
        }

        if ($query){
            $response = array(
                'status' => 200,
                'message' => "success",
                'data' => $list_transaksi,
            );
        }else{
            die('Error: '. mysqli_error($query));
        }

        header('Content-Type: application/json');
        echo json_encode($response);

    }

    function get_transaksi_by_status_pencairan(){
        global $connect;
        $status_pencairan = $_GET['status_pencairan'];
        $status_transaksi = $_GET['status_transaksi'];
        $id_dropshipper = $_GET['id_dropshipper'];
        $list_transaksi = array();
        $query = mysqli_prepare($connect, 
        "SELECT * FROM transaksi WHERE status_transaksi= ? AND status_pencairan=? AND id_dropshipper = ?");
        mysqli_stmt_bind_param($query, 'ssi',$status_transaksi, $status_pencairan, $id_dropshipper);
        mysqli_stmt_execute($query);
        $result = mysqli_stmt_get_result($query);

        while($row = mysqli_fetch_array($result))
        {
            $data = array();
            $data["id_transaksi"] = (int)$row["id_transaksi"];
            $data["status_transaksi"] =$row["status_transaksi"]; 
             $data["status_pencairan"] =$row["status_pencairan"]; 
            $data["nama_pembeli"]=$row["nama_pembeli"];
            $data["alamat"]=$row["alamat"];
            $data["no_hp"]=$row["no_hp"];
            $data["nama_pengirim"]=$row["nama_pengirim"];
            $data["no_hp_pengirim"]=$row["no_hp_pengirim"];
            $data["jasa_pengiriman"]=$row["jasa_pengiriman"];
            $data["grand_total"]= (int)$row["grand_total"];
            $data["harga_jual"]= (int)$row["harga_jual"];
            $data["photo_dikirim_path"]=$row["photo_dikirim_path"];
            $data["no_resi"]=$row["no_resi"];
            $data["tanggal_transaksi"]=$row["created_at"];
            $data["id_dropshipper"] =$row["id_dropshipper"];
            $data["pesanan"] = array();
            
            $get_list_barang = mysqli_query($connect, "SELECT * FROM transaksi_barang WHERE id_transaksi=" .$row["id_transaksi"]);

            while($pesanan = mysqli_fetch_assoc($get_list_barang)){
                $data["pesanan"][] = array(
                    "id_barang" => (int)$pesanan["id_barang"],
                    "nama" => $pesanan["nama"],
                    "berat" => (int)$pesanan["berat"],
                    "jumlah" => (int)$pesanan["jumlah"],
                    "deskripsi" => $pesanan["deskripsi"],
                    "total_harga" => (int)$pesanan["total_harga"],
                    "harga_satuan" => (int)$pesanan["harga_satuan"],
                );
            }
            array_push($list_transaksi, $data);
        }

        if ($query){
            $response = array(
                'status' => 200,
                'message' => "success",
                'data' => $list_transaksi,
            );
        }else{
            die('Error: '. mysqli_error($query));
        }

        header('Content-Type: application/json');
        echo json_encode($response);

    }


    function get_transaksi_by_id(){
        
        global $connect;
        $id_transaksi = $_GET['id_transaksi'];
        $id_dropshipper = $_GET['id_dropshipper'];
        $list_pesanan = [];
        $query = mysqli_prepare($connect,"SELECT * FROM transaksi WHERE id_transaksi=? AND id_dropshipper = ?");
        mysqli_stmt_bind_param($query, 'ii', $id_transaksi, $id_dropshipper);
        mysqli_stmt_execute($query);
        $result = mysqli_stmt_get_result($query);
      
        while($row = mysqli_fetch_array($result))
        {
            $data["id_transaksi"] = (int)$row["id_transaksi"];
            $data["status_transaksi"] =$row["status_transaksi"]; 
            $data["nama_pembeli"]=$row["nama_pembeli"];
            $data["alamat"]=$row["alamat"];
            $data["no_hp"]=$row["no_hp"];
            $data["nama_pengirim"]=$row["nama_pengirim"];
            $data["no_hp_pengirim"]=$row["no_hp_pengirim"];
            $data["jasa_pengiriman"]=$row["jasa_pengiriman"];
            $data["grand_total"]= (int)$row["grand_total"];
            $data["harga_jual"]= (int)$row["harga_jual"];
            $data["photo_dikirim_path"]=$row["photo_dikirim_path"];
            $data["no_resi"]=$row["no_resi"];
            $data["tanggal_transaksi"]=$row["created_at"];
            $data["status_pencairan"] = $row["status_pencairan"];
            $data["bukti_transfer_path"] = $row["bukti_transfer_path"];
            $data["pesanan"] = array();
            
            $get_list_barang = mysqli_query($connect, "SELECT * FROM transaksi_barang WHERE id_transaksi=" .$row["id_transaksi"]);
            while($pesanan = mysqli_fetch_assoc($get_list_barang)){
                $data["pesanan"][] = array(
                    "id_barang" => (int)$pesanan["id_barang"],
                    "nama" => $pesanan["nama"],
                    "berat" => (int)$pesanan["berat"],
                    "jumlah" => (int)$pesanan["jumlah"],
                    "deskripsi" => $pesanan["deskripsi"],
                    "total_harga" => (int)$pesanan["total_harga"],
                    "harga_satuan" => (int)$pesanan["harga_satuan"],
                );
            }
            array_push($list_pesanan, $data);

        }
        
        if ($query){
            $response = array(
                'status' => 200,
                'message' => "success",
                'data' => $list_pesanan,
            );
        }else{
            die('Error: '. mysqli_error($query));
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    
    }

    function get_transaksi_by_dropshipper(){
        
        global $connect;
        $id = $_GET['id_dropshipper'];
        $list_pesanan = [];
        $query = mysqli_query($connect,"SELECT * FROM transaksi WHERE id_dropshipper=". $id);
      
        while($row = mysqli_fetch_array($query))
        {
            $data["id_transaksi"] = (int)$row["id_transaksi"];
            $data["status_transaksi"] =$row["status_transaksi"]; 
            $data["nama_pembeli"]=$row["nama_pembeli"];
            $data["alamat"]=$row["alamat"];
            $data["no_hp"]=$row["no_hp"];
            $data["nama_pengirim"]=$row["nama_pengirim"];
            $data["no_hp_pengirim"]=$row["no_hp_pengirim"];
            $data["jasa_pengiriman"]=$row["jasa_pengiriman"];
            $data["grand_total"]= (int)$row["grand_total"];
            $data["harga_jual"]= (int)$row["harga_jual"];
            $data["photo_dikirim_path"]=$row["photo_dikirim_path"];
            $data["tanggal_transaksi"]=$row["created_at"];
            $data["no_resi"]=$row["no_resi"];
            $data["pesanan"] = array();
            
            $get_list_barang = mysqli_query($connect, "SELECT * FROM transaksi_barang WHERE id_transaksi=" .$row["id_transaksi"]);
            while($pesanan = mysqli_fetch_assoc($get_list_barang)){
                // $get_product_photo = mysqli_query($connect, "SELECT foto_produk FROM produk WHERE id_produk =" .$pesanan["id_barang"]);
                
                $data["pesanan"][] = array(
                    "id_barang" => (int)$pesanan["id_barang"],
                    "nama" => $pesanan["nama"],
                    "berat" => (int)$pesanan["berat"],
                    "jumlah" => (int)$pesanan["jumlah"],
                    "deskripsi" => $pesanan["deskripsi"],
                    "total_harga" => (int)$pesanan["total_harga"],
                    "harga_satuan" => (int)$pesanan["harga_satuan"],
                );
            }
            array_push($list_pesanan, $data);

        }
        
        if ($query){
            $response = array(
                'status' => 200,
                'message' => "success",
                'data' => $list_pesanan,
            );
        }else{
            die('Error: '. mysqli_error($query));
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    
    }

    function admin_get_transaksi_by_status(){
        global $connect;
        $status_transaksi = $_GET['status'];
        $list_transaksi = array();
        $query = mysqli_prepare($connect, 
        "SELECT * FROM transaksi WHERE status_transaksi = ? ");
        mysqli_stmt_bind_param($query, 's', $status_transaksi);
        mysqli_stmt_execute($query);
        $result = mysqli_stmt_get_result($query);

        while($row = mysqli_fetch_array($result))
        {
            $data = array();
            $data["id_transaksi"] = (int)$row["id_transaksi"];
            $data["id_dropshipper"] = (int)$row["id_dropshipper"];
            $data["status_transaksi"] =$row["status_transaksi"]; 
            $data["nama_pembeli"]=$row["nama_pembeli"];
            $data["alamat"]=$row["alamat"];
            $data["no_hp"]=$row["no_hp"];
            $data["nama_pengirim"]=$row["nama_pengirim"];
            $data["no_hp_pengirim"]=$row["no_hp_pengirim"];
            $data["jasa_pengiriman"]=$row["jasa_pengiriman"];
            $data["grand_total"]= (int)$row["grand_total"];
            $data["harga_jual"]= (int)$row["harga_jual"];
            $data["photo_dikirim_path"]=$row["photo_dikirim_path"];
            $data["tanggal_transaksi"]=$row["created_at"];
            $data["no_resi"]=$row["no_resi"];
            $data["pesanan"] = array();
            
            $get_list_barang = mysqli_query($connect, "SELECT * FROM transaksi_barang WHERE id_transaksi=" .$row["id_transaksi"]);

            while($pesanan = mysqli_fetch_assoc($get_list_barang)){
                $data["pesanan"][] = array(
                    "id_barang" => (int)$pesanan["id_barang"],
                    "nama" => $pesanan["nama"],
                    "berat" => (int)$pesanan["berat"],
                    "jumlah" => (int)$pesanan["jumlah"],
                    "deskripsi" => $pesanan["deskripsi"],
                    "total_harga" => (int)$pesanan["total_harga"],
                    "harga_satuan" => (int)$pesanan["harga_satuan"],
                );
            }
            array_push($list_transaksi, $data);
        }
        
        if ($result){
            $response = array(
                'status' => 200,
                'message' => "success",
                'data' => $list_transaksi,
            );
        }else{
            die('Error: '. mysqli_error($query));
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function get_transaksi_by_status(){
        global $connect;
        $status_transaksi = $_GET['status'];
        $id_dropshipper = $_GET['id_dropshipper'];
        $list_transaksi = array();
        $query = mysqli_prepare($connect, 
        "SELECT * FROM transaksi WHERE status_transaksi = ? AND id_dropshipper= ?");
        mysqli_stmt_bind_param($query, 'si', $status_transaksi, $id_dropshipper);
        mysqli_stmt_execute($query);
        $result = mysqli_stmt_get_result($query);

        while($row = mysqli_fetch_array($result))
        {
            $data = array();
            $data["id_transaksi"] = (int)$row["id_transaksi"];
            $data["status_transaksi"] =$row["status_transaksi"]; 
            $data["nama_pembeli"]=$row["nama_pembeli"];
            $data["alamat"]=$row["alamat"];
            $data["no_hp"]=$row["no_hp"];
            $data["nama_pengirim"]=$row["nama_pengirim"];
            $data["no_hp_pengirim"]=$row["no_hp_pengirim"];
            $data["jasa_pengiriman"]=$row["jasa_pengiriman"];
            $data["grand_total"]= (int)$row["grand_total"];
            $data["harga_jual"]= (int)$row["harga_jual"];
            $data["photo_dikirim_path"]=$row["photo_dikirim_path"];
            $data["tanggal_transaksi"]=$row["created_at"];
            $data["no_resi"]=$row["no_resi"];
            $data["pesanan"] = array();
            
            $get_list_barang = mysqli_query($connect, "SELECT * FROM transaksi_barang WHERE id_transaksi=" .$row["id_transaksi"]);

            while($pesanan = mysqli_fetch_assoc($get_list_barang)){
                $data["pesanan"][] = array(
                    "id_barang" => (int)$pesanan["id_barang"],
                    "nama" => $pesanan["nama"],
                    "berat" => (int)$pesanan["berat"],
                    "jumlah" => (int)$pesanan["jumlah"],
                    "deskripsi" => $pesanan["deskripsi"],
                    "total_harga" => (int)$pesanan["total_harga"],
                    "harga_satuan" => (int)$pesanan["harga_satuan"],
                );
            }
            array_push($list_transaksi, $data);
        }
        
        if ($result){
            $response = array(
                'status' => 200,
                'message' => "success",
                'data' => $list_transaksi,
            );
        }else{
            die('Error: '. mysqli_error($query));
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function ubah_status_transaksi(){
        global $connect;
        $id = $_POST['id'];
        $status_transaksi = $_POST['status_transaksi'];
        $query = mysqli_query($connect, "UPDATE transaksi SET
        status_transaksi = '".$status_transaksi."',
        WHERE id_transaksi=".$id);

        if($query){
            $response = array(
                'status' => 200,
                'message' => "Success",
            );
        }else{
            die('Error: '.mysqli_error($query));
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function delete_transaksi(){
        global $connect;
        $id = $_GET['id'];
        $query = mysqli_query($connect,"DELETE FROM transaksi WHERE id_transaksi=". $id);
        if ($query){
            $response = array(
                'status' => 200,
                'message' => "data berhasil dihapus.",
            );
        }else{
            die('Error: '. mysqli_error($query));
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }
?>