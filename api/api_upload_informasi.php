<?php
    define('__ROOT__', dirname(dirname(__FILE__)));
    require_once __ROOT__.'/database/koneksi.php';
    require '../vendor/autoload.php';


    if(function_exists($_GET['action'])) {
         $_GET['action']();
    }  

    function download_all_dropshipper(){
        global $connect;

        $data = array(
            array(
                'Nama Dropshipper',
                'Email',
                'Gender',
                'Alamat',
                'No. Hp',
                'Nama Rekening',
                'No. Rekening',
                'Bank'
            )
        );

        $dropshipper_level_registered = 2;

        $get_dropshipper = mysqli_prepare($connect, "SELECT 
            nama_ds,
            email,
            gender,
            alamat,
            no_hp,
            nama_rek,
            no_rek,
            bank
            FROM dropshipper
            WHERE level = ?
        ");
        mysqli_stmt_bind_param($get_dropshipper, 'i', $dropshipper_level_registered);

        mysqli_stmt_execute($get_dropshipper);
        $result = mysqli_stmt_get_result($get_dropshipper);

        while($row = mysqli_fetch_row($result)){
            $data[] = $row;
        }

        if($result){
            $response = array(
                'status' => 200,
                'message' => "success",
                'data' => $data
            );
        }else{
            die("Error.");
        }
        $xlsx = Shuchkin\SimpleXLSXGen::fromArray( $data );
        $xlsx->downloadAs('dropshipper.xlsx'); 

        //    header('Content-Type: application/json');
        //  echo json_encode($response);
    

    }

    function download_all_produk(){
        global $connect;

        $data = array(
            array(
                'Nama Produk',
                'Harga Pokok',
                'Deskripsi',
                'Berat'
            )
        );

        $get_produk = mysqli_prepare($connect, "SELECT 
            nama_produk,
            harga_pokok,
            deskripsi,
            berat
            FROM produk
        ");

        mysqli_stmt_execute($get_produk);
        $result = mysqli_stmt_get_result($get_produk);

        while($row = mysqli_fetch_row($result)){
            $data[] = $row;
        }

        if($result){
            $response = array(
                'status' => 200,
                'message' => "success",
                'data' => $data
            );
        }else{
            die("Error.");
        }
        $xlsx = Shuchkin\SimpleXLSXGen::fromArray( $data );
        $xlsx->downloadAs('produk.xlsx'); 

        //  header('Content-Type: application/json');
        //  echo json_encode($response);
        

    }   

    function download_fee_transaksi(){
        global $connect;

        $data = array(
            array(
                'Nama Pembeli', 
                'Nama Pengirim',
                'Status Transaksi',
                'Status Pencairan',
                'Jasa Pengiriman', 
                'No. Resi',
                'Grand Total',
                'Harga Jual',
                'No. Hp',
                'No. Hp Pengirim',
                'Alamat',
                'Nama Barang',
                'Jumlah Barang',
                'Berat Barang'
            ),
        );

        $get_transaksi = mysqli_prepare($connect, "SELECT
        transaksi.nama_pembeli,
        transaksi.nama_pengirim,
        transaksi.status_transaksi,
        transaksi.status_pencairan,
        transaksi.jasa_pengiriman,
        transaksi.no_resi,
        transaksi.grand_total,
        transaksi.harga_jual,
        transaksi.no_hp,
        transaksi.no_hp_pengirim,
        transaksi.alamat,
        transaksi_barang.nama,
        transaksi_barang.jumlah,
        transaksi_barang.berat
        FROM transaksi
        INNER JOIN transaksi_barang ON transaksi.id_transaksi = transaksi_barang.id_transaksi
        WHERE transaksi.status_pencairan = 'DICAIRKAN';
        ");

        mysqli_stmt_execute($get_transaksi);
        $result = mysqli_stmt_get_result($get_transaksi);

        while($row = mysqli_fetch_row($result)){
            $data[] = $row;
        }

        if($result){
            $response = array(
                'status' => 200,
                'message' => "success",
                'data' => $data
            );
        }else{
            die("Error.");
        }
        $xlsx = Shuchkin\SimpleXLSXGen::fromArray( $data );
        $xlsx->downloadAs('fee_transaksi.xlsx'); 

        // header('Content-Type: application/json');
        // echo json_encode($response);
    }

   

    function download_all_transaksi(){
        global $connect;

        $data = array(
            array(
                'Nama Pembeli', 
                'Nama Pengirim',
                'Status Transaksi',
                'Status Pencairan',
                'Jasa Pengiriman', 
                'No. Resi',
                'Grand Total',
                'Harga Jual',
                'No. Hp',
                'No. Hp Pengirim',
                'Alamat',
                'Nama Barang',
                'Jumlah Barang',
                'Berat Barang'
            ),
        );

        $get_transaksi = mysqli_prepare($connect, "SELECT
        transaksi.nama_pembeli,
        transaksi.nama_pengirim,
        transaksi.status_transaksi,
        transaksi.status_pencairan,
        transaksi.jasa_pengiriman,
        transaksi.no_resi,
        transaksi.grand_total,
        transaksi.harga_jual,
        transaksi.no_hp,
        transaksi.no_hp_pengirim,
        transaksi.alamat,
        transaksi_barang.nama,
        transaksi_barang.jumlah,
        transaksi_barang.berat
        FROM transaksi
        INNER JOIN transaksi_barang ON transaksi.id_transaksi = transaksi_barang.id_transaksi
        WHERE transaksi.status_transaksi = 'DIKIRIM' OR transaksi.status_transaksi = 'SELESAI_DIPROSES';
        ");

        mysqli_stmt_execute($get_transaksi);
        $result = mysqli_stmt_get_result($get_transaksi);

        while($row = mysqli_fetch_row($result)){
            $data[] = $row;
        }

        if($result){
            $response = array(
                'status' => 200,
                'message' => "success",
                'data' => $data
            );
        }else{
            die("Error.");
        }
        $xlsx = Shuchkin\SimpleXLSXGen::fromArray( $data );
        $xlsx->downloadAs('transaksi.xlsx'); 

        // header('Content-Type: application/json');
        // echo json_encode($response);
    }

    function get_all_informasi(){
        global $connect;
        $data = array();

        $query = mysqli_prepare($connect, "SELECT * FROM gudang_informasi");
        mysqli_stmt_execute($query);
        $result = mysqli_stmt_get_result($query);

        while($row = mysqli_fetch_object($result))
        {
            $data[] = $row;
        }

        if($query){
            $response = array(
                'status' => 200,
                'message' => "success",
                'data' => $data
            );
        }else{
            die('Error: '. mysqli_error($query));

        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function upload_informasi(){
        global $connect;
        $nama_informasi = $_POST['nama_informasi'];
        $deskripsi = $_POST['deskripsi'];
        $kode_inf = $_POST['kode_inf'];
        $file = $_FILES['file'];
        
        $file_name = $_FILES['file']['name'];
        $target_folder = __ROOT__."/upload/". $file_name;
        $file_type = $_FILES['file']['type'];
        $protocol = $_SERVER['PROTOCOL'] = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https' : 'http';
        $path = $protocol . "://" . $_SERVER['SERVER_NAME']."/dropshot"."/upload/".$file_name;


        if ($file_type=="application/pdf") {
            if(move_uploaded_file($file['tmp_name'], $target_folder))
            {
                $query = mysqli_prepare($connect, "INSERT INTO gudang_informasi(
                    nama_inf,
                    deskripsi,
                    kode_inf,
                    file_dok
                )
                VALUES(
                    ?,
                    ?,
                    ?,
                    ?
                )");
                mysqli_stmt_bind_param($query, 'ssss', $nama_informasi, $deskripsi, $kode_inf, $path);
                mysqli_stmt_execute($query);
                $result = mysqli_stmt_get_result($query);
                if($query){
                    $response = array(
                        'status' => 200,
                        'message' => "success",
                    );
                }else{
                    die('Error');
                }
                header('Content-Type: application/json');
            echo json_encode($response);
            }
            else {
                echo "Problem uploading file";
            }
            
        }
        else {
            echo "You may only upload PDFs, JPEGs or GIF files.<br>";
        }
    }

    function get_folder_path($id_informasi){
        global $connect;

        $get_url_string = mysqli_prepare($connect, "SELECT file_dok FROM gudang_informasi WHERE id_inf = ?");
        mysqli_stmt_bind_param($get_url_string, 'i', $id_informasi);
        mysqli_stmt_execute($get_url_string);
        $result = mysqli_stmt_get_result($get_url_string);
        $url_string = '';
        while($row = mysqli_fetch_array($result)){
            $url_string = $row['file_dok'];
        }

        $protocol = parse_url($url_string, PHP_URL_SCHEME)."://";
        $domain = parse_url($url_string, PHP_URL_HOST)."/";

        $folder_path = str_replace($protocol.$domain, '/', $url_string);
        return $folder_path;


    }

    function delete_informasi(){
        global $connect;

        $id_informasi = $_GET['id_informasi'];
        $path = get_folder_path($id_informasi);
        unlink($_SERVER["DOCUMENT_ROOT"].$path);
        $delete_informasi = mysqli_prepare($connect, "DELETE FROM gudang_informasi WHERE id_inf = ?");
        mysqli_stmt_bind_param($delete_informasi, 'i', $id_informasi);
        mysqli_stmt_execute($delete_informasi);

        if($delete_informasi){
            $response = array(
                'status' => 200,
                'message' => "success",
            );
        }else{
            die("Error.");
        }

        header("Content-Type: application/json");
        echo json_encode($response);

    }
?>