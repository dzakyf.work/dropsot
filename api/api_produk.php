<?php 
    define('__ROOT__', dirname(dirname(__FILE__)));
    require_once __ROOT__.'/database/koneksi.php';

    if(function_exists($_GET['action'])) {
         $_GET['action']();
      }   

    function insert_product(){
        global $connect;

        $nama_produk = $_POST['nama_produk'];
        $deskripsi = $_POST['deskripsi'];
        $harga_pokok = $_POST['harga_pokok'];
        $harga_jual = $_POST['harga_jual'];

        $foto = $_FILES['foto'];
        $foto_name = $_FILES['foto']['name'];
        $protocol = $_SERVER['PROTOCOL'] = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https' : 'http';
        $path = $protocol . "://" . $_SERVER['SERVER_NAME']."/dropshot"."/foto/upload/".$foto_name;
        move_uploaded_file($foto['tmp_name'], __ROOT__."/foto/upload/". $foto_name);

        $result = mysqli_query($connect, "INSERT INTO produk(
            nama_produk,
            deskripsi,
            harga_pokok,
            harga_jual,
            foto_produk
            )
            VALUES(
            '$nama_produk',
            '$deskripsi',
            '$harga_pokok',
            '$harga_jual',
            '$path')"
            );
        
        if($result){
            $response = array(
                'status' => 200,
                'message' => "success",
            );
        }else{
            die('Error: '. mysqli_error($result));
        }
        
         header('Content-Type: application/json');
         echo json_encode($response);

    }

    function update_product()
    {
        global $connect;
        $id = $_GET['id'];
        
        // $json = json_decode(file_get_contents('php://input'), true);
        $nama_produk = $_POST['nama_produk'];
        $deskripsi = $_POST['deskripsi'];
        $harga_pokok = $_POST['harga_pokok'];
        $harga_jual = $_POST['harga_jual'];

        $foto = $_FILES['foto'];
        $foto_name = $_FILES['foto']['name'];
        $protocol = $_SERVER['PROTOCOL'] = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https' : 'http';
        $path = $protocol . "://" . $_SERVER['SERVER_NAME']."/dropshot"."/foto/upload/".$foto_name;
        move_uploaded_file($foto['tmp_name'], __ROOT__."/foto/upload/". $foto_name);
        
        $query = mysqli_query($connect,"UPDATE produk SET
        nama_produk = '".$nama_produk."',
        deskripsi = '".$deskripsi."',
        harga_pokok = '".$harga_pokok."',
        harga_jual = '".$harga_jual."',
        foto_produk = '".$path."'
        WHERE id_produk=". $id);

        if ($query){
             $response = array(
                'status' => 200,
                'message' => "success",
            );
        }else{
            die('Error: '. mysqli_error($result));
        }

        header('Content-Type: application/json');
        echo json_encode($response);

    }

    function delete_product()
    {
        global $connect;
        $id = $_GET['id'];
        $query = mysqli_query($connect,"DELETE FROM produk WHERE id_produk=". $id);
        if ($query){
            $response = array(
                'status' => 200,
                'message' => "data berhasil dihapus.",
            );
        }else{
            die('Error: '. mysqli_error($query));
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function get_product_by_id()
    {
        global $connect;
        $id = $_GET['id'];
        $data = [];
        $query = mysqli_query($connect,"SELECT * FROM produk WHERE id_produk=". $id);
      
        while($row = mysqli_fetch_object($query))
        {
            $data[] = $row;
        }
        
        if ($query){
            $response = array(
                'status' => 200,
                'message' => "success",
                'data' => $data,
            );
        }else{
            die('Error: '. mysqli_error($query));
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function get_all_product()
    {
        global $connect;
        $data = array();

        $query = mysqli_query($connect, "SELECT * FROM produk");

        while($row = mysqli_fetch_object($query))
        {
            $data[] = $row;
        }

        if($query){
            $response = array(
                'status' => 200,
                'message' => "success",
                'data' => $data
            );
        }else{
            die('Error: '. mysqli_error($query));

        }

        header('Content-Type: application/json');
        echo json_encode($response);

    }
?>