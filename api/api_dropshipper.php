<?php 
    define('__ROOT__', dirname(dirname(__FILE__)));
    require_once __ROOT__.'/database/koneksi.php';

    if(function_exists($_GET['action'])) {
         $_GET['action']();
      }   

    
    function get_dropshipper_by_id()
    {
        global $connect;
        $id_ds = $_GET["id_dropshipper"];
        $data = [];

        $get_dropshipper = mysqli_prepare($connect, "SELECT * FROM dropshipper WHERE id_ds = ?");
        mysqli_stmt_bind_param($get_dropshipper, 'i', $id_ds);
        mysqli_stmt_execute($get_dropshipper);
        $result = mysqli_stmt_get_result($get_dropshipper);

        while($row = mysqli_fetch_object($result)){
            $data[] = $row;
        }

        if ($result){
            $response = array(
                'status' => 200,
                'message' => "success",
                'data' => $data,
            );
        }else{
            die('Error: '. mysqli_error($get_dropshipper));
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function get_dropshipper_by_level()
    {
        global $connect;
        $level = $_GET['level'];
        $data = [];
        $query = mysqli_query($connect,"SELECT * FROM dropshipper WHERE level=". $level);
      
        while($row = mysqli_fetch_object($query))
        {
            $data[] = $row;
        }
        
        if ($query){
            $response = array(
                'status' => 200,
                'message' => "success",
                'data' => $data,
            );
        }else{
            die('Error: '. mysqli_error($query));
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function confirm_dropshipper(){
         global $connect;
         $id = $_GET['id'];
         $query = mysqli_query($connect, "UPDATE dropshipper SET
         level = 2
         WHERE id_ds=". $id);

         if($query){
            $response = array(
                'status' => 200,
                'message' => "success mengkonfirmasi dropshipper",
                
            );
        }else{
            die('Error: '. mysqli_error($query));
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }
?>