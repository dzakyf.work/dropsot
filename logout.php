<?php
session_start();
session_destroy();

if (isset($_SERVER['HTTP_COOKIE'])){
    $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
    foreach($cookies as $cookie){
        $parts = explode('=', $cookie);
        $name = trim($parts[0]);
        setcookie($name, '', time()-3600);
        setcookie($name, '', time()-3600, '/');
    }
}
header("location:index.php");
?>